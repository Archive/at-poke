/*
 * accessible-tree-model.c: A tree model for Accessibles
 *
 * Authors:
 *    Michael Meeks
 *
 * Copyright 2002 Sun Microsystems, Inc.
 */
#include <config.h>
#include <stdio.h>
#include <string.h>
#include <bonobo/bonobo-exception.h>

#include "graphics.h"
#include "accessible-tree-model.h"

#undef DEBUG_TREE

/* Java requires disabling lots of sanity checks :-( */
#define JAVA_ENABLED

typedef enum {
	NODE_GOT_CHILDREN = 1,
	NODE_CHILDREN_CHANGED = 2,
	NODE_FETCHING_CHILDREN = 4
} ModelNodeState;

typedef struct {
	int             ref;
	ModelNodeState  state;
	Accessible     *accessible;

	/* Cache for later */
	char           *name;
	char           *description;
	AccessibleRole  role;
	char           *role_name; /* sad but true */
	gboolean        display_children;
} ModelNode;

extern int      max_children;
extern gboolean dontdisplaychildren;
extern gboolean use_table_if;

static GNode *
model_node_new (AccessibleTreeModel *model, Accessible *accessible)
{
	GNode *node;
	ModelNode *mnode = g_new0 (ModelNode, 1);

	mnode->role = SPI_ROLE_INVALID;
	mnode->accessible = accessible;
	Accessible_ref (accessible);

	mnode->display_children = TRUE;
	if (dontdisplaychildren) {
		AccessibleStateSet *state_set;

		state_set = Accessible_getStateSet (accessible);
		if (AccessibleStateSet_contains (state_set,
						 SPI_STATE_MANAGES_DESCENDANTS)) {
			mnode->display_children = FALSE;
		}
		AccessibleStateSet_unref (state_set);
	}
	node = g_node_new (mnode);

	g_hash_table_insert (model->accessible_to_node, accessible, node);

	return node;
}

static void
mnode_destroy (AccessibleTreeModel *model, ModelNode *mnode)
{
	g_hash_table_remove (model->accessible_to_node, mnode->accessible);
	Accessible_unref (mnode->accessible);
		
	SPI_freeString (mnode->name);
	SPI_freeString (mnode->description);
	SPI_freeString (mnode->role_name);
		
	g_free (mnode);
}

void
model_construct (AccessibleTreeModel *model, Accessible *root)
{
	model->accessible_to_node = g_hash_table_new (NULL, NULL);
	model->root = model_node_new (model, root);
}

static GObjectClass *parent_class;

#define ITER_NODE(i)  ((GNode *)((i)->user_data))
#define ITER_NODE_L(i) ((i)->user_data)
#define MODEL_NODE(n) ((ModelNode *)(n)->data)

static gboolean
make_iter_invalid (GtkTreeIter *iter)
{
	iter->stamp = 0;
	iter->user_data = NULL;
	iter->user_data2 = NULL;
	iter->user_data3 = NULL;

	return FALSE;
}

static gboolean
make_iter (GtkTreeModel *model, GNode *node, GtkTreeIter *iter)
{
	if (!node)
		return make_iter_invalid (iter);

	iter->stamp = ACCESSIBLE_TREE_MODEL (model)->stamp;
	ITER_NODE_L (iter) = node;
	iter->user_data2 = NULL;
	iter->user_data3 = NULL;

	return TRUE;
}

static void
model_node_inserted (GtkTreeModel *model, GNode *node)
{
	GtkTreeIter  iter;
	GtkTreePath *path;

	make_iter (model, node, &iter);
	path = gtk_tree_model_get_path (model, &iter);

	gtk_tree_model_row_inserted (model, path, &iter);
	gtk_tree_model_row_has_child_toggled (model, path, &iter);

	gtk_tree_path_free (path);
}

static void
model_node_changed (GtkTreeModel *model, GNode *node)
{
	GtkTreeIter  iter;
	GtkTreePath *path;

	make_iter (model, node, &iter);
	path = gtk_tree_model_get_path (model, &iter);

	gtk_tree_model_row_changed (model, path, &iter);

	gtk_tree_path_free (path);
}

static void
model_node_destroy (GtkTreeModel *model, GNode *node)
{
	GtkTreeIter  iter;
	GtkTreePath *path;

	while (node->children)
		model_node_destroy (model, node->children);

	make_iter (model, node, &iter);
	path = gtk_tree_model_get_path (model, &iter);

	mnode_destroy (ACCESSIBLE_TREE_MODEL (model), node->data);
	g_node_destroy (node);

	gtk_tree_model_row_deleted (model, path);

	gtk_tree_path_free (path);
}

static gboolean
model_node_has_children (GtkTreeModel *tree_model, GNode *node)
{
	ModelNode *mnode;
	Accessible *accessible;
	gboolean retval;
	AccessibleStateSet *stateset;

	g_return_val_if_fail (node != NULL, FALSE);

	mnode = node->data;

	if (!mnode->display_children)
		return FALSE;

	if (mnode->state & NODE_GOT_CHILDREN)
		return node->children != NULL;

	accessible = Accessible_getChildAtIndex (mnode->accessible, 0);
	retval = accessible != NULL;
	if (accessible)
		Accessible_unref (accessible);

	return retval;
}

static void
model_node_sync_children (GtkTreeModel *tree_model, GNode *node)
{
	int i, j;
	GNode  *lnode;
	GSList *children, *l;
	ModelNode *mnode;
	AccessibleTreeModel *model = ACCESSIBLE_TREE_MODEL (tree_model);
	AccessibleTable *accessible_table = NULL;
	long n_rows, n_cols;

	g_return_if_fail (node != NULL);

	mnode = node->data;

	if (mnode->state & NODE_GOT_CHILDREN)
		return;

	if (use_table_if)
		accessible_table = Accessible_getTable (mnode->accessible);

 retry_on_reenter:
	mnode->state |= NODE_GOT_CHILDREN | NODE_FETCHING_CHILDREN;

	model->stamp++;
	children = NULL;

	if (accessible_table) {
		if (!max_children) {
			n_rows = AccessibleTable_getNRows (accessible_table);
			n_cols = AccessibleTable_getNColumns (accessible_table);
		} else {
			n_rows = max_children;
			n_cols = max_children;
		}
		for (i = 0; i < n_rows; i++) {
			for (j = 0; j < n_cols; j++) {
				Accessible *accessible;

				accessible = AccessibleTable_getAccessibleAt (accessible_table, i, j);
				if (mnode->state & NODE_CHILDREN_CHANGED) {
					mnode->state &= ~NODE_CHILDREN_CHANGED;
					for (l = children; l; l = l->next)
						Accessible_unref (l->data);
					g_slist_free (children);
					goto retry_on_reenter;
				}

				if (!accessible)
					break;

				children = g_slist_prepend (children, accessible);
			}
		}
		Accessible_unref (accessible_table);
	} else {
	
		for (i = 0; max_children ? i < max_children : TRUE; i++) {
			Accessible *accessible;

			accessible = Accessible_getChildAtIndex (mnode->accessible, i);

			if (mnode->state & NODE_CHILDREN_CHANGED) {
				mnode->state &= ~NODE_CHILDREN_CHANGED;
				for (l = children; l; l = l->next)
					Accessible_unref (l->data);
				g_slist_free (children);
				goto retry_on_reenter;
			}

			if (!accessible)
				break;

			children = g_slist_prepend (children, accessible);
		}
	}
	mnode->state &= ~NODE_FETCHING_CHILDREN;

	children = g_slist_reverse (children);

#ifdef DEBUG_TREE
	g_warning ("Fetched %d children ... for '%s'",
		   g_slist_length (children),
		   mnode->name ? mnode->name : "<noname>");
#endif

	/* Reconcile what we have now with what we had then */
	lnode = node->children;
	l = children;
	for (; l || lnode; l = l ? l->next : NULL) {

		if (!l) {
			GNode *next;

#ifdef DEBUG_TREE
			g_warning ("Model node '%s' removed !", MODEL_NODE (lnode)->name);
#endif
			next = lnode->next;
			model_node_destroy (tree_model, lnode);
			lnode = next;

		} else if (lnode && l->data == MODEL_NODE (lnode)->accessible) {
			Accessible_unref (l->data);
			model_node_changed (tree_model, lnode);
			lnode = lnode->next;

		} else {
			GNode *child;

			child = model_node_new (model, l->data);
			Accessible_unref (l->data);

			g_node_insert_before (node, lnode, child);
			model_node_inserted (tree_model, child);
		}
	}

	g_slist_free (children);
}

static void
model_node_children_changed (AccessibleTreeModel *tree_model, Accessible *source)
{
	GNode *node;
	ModelNode *mnode;

	node = g_hash_table_lookup (tree_model->accessible_to_node, source);

	if (!node) {
		/*
		 * This is not neccesarily an error - quite possibly
		 * we're getting events from a tree we're not interested
		 * in or a tree which is in the process of being deleted
		 * which is quite ok.
		 */
#if DEBUG_TREE
		g_warning ("Changed ('%p') in another tree model !", source);
#endif
		return;
	}

	if (!(mnode = node->data)) {
		g_warning ("Empty node");
		return;
	}

	if (!mnode->display_children)
		return;

	if (mnode->state & NODE_FETCHING_CHILDREN) {
		mnode->state |= NODE_CHILDREN_CHANGED;
		g_warning ("Heavy duty re-entrancy on node '%p'", node);
	} else {
		mnode->state &= ~NODE_GOT_CHILDREN;
		model_node_sync_children (GTK_TREE_MODEL (tree_model), node);
	}
}

static int
accessible_tree_model_get_n_columns (GtkTreeModel *model)
{
	return ACCESSIBLE_TREE_MODEL_NUM_COLS;
}

static GType
accessible_tree_model_get_column_type (GtkTreeModel *model, int index)
{
	switch (index) {
	case ACCESSIBLE_TREE_MODEL_NAME_COL:
		return G_TYPE_STRING;
	case ACCESSIBLE_TREE_MODEL_ROLE_COL:
		return G_TYPE_STRING;
	case ACCESSIBLE_TREE_MODEL_DESCR_COL:
		return G_TYPE_STRING;
	case ACCESSIBLE_TREE_MODEL_ICON_COL:
		return GDK_TYPE_PIXBUF;
	default:
		g_assert_not_reached ();
	}
	
	return G_TYPE_INVALID;
}

static gboolean
accessible_tree_model_get_iter (GtkTreeModel *model,
				GtkTreeIter  *iter,
				GtkTreePath  *path)
{
	int depth, i;
	int *indices;
	GtkTreeIter parent;

	depth = gtk_tree_path_get_depth (path);
	indices = gtk_tree_path_get_indices (path);

	if (!gtk_tree_model_iter_nth_child (model, iter, NULL, indices [0])) {
		g_warning ("Foo !");
		return FALSE;
	}

	for (i = 1; i < depth; i++) {
		parent = *iter;

		if (!gtk_tree_model_iter_nth_child (model, iter,
						    &parent, indices [i])) {
			g_warning ("get_iter returns false");
			return FALSE;
		}
	}

	return TRUE;
}

static GtkTreePath *
accessible_tree_model_get_path (GtkTreeModel *model, GtkTreeIter *iter)
{
	GNode *node;
	GtkTreePath *path;
	AccessibleTreeModel *tree_model;

	tree_model = ACCESSIBLE_TREE_MODEL (model);

	path = gtk_tree_path_new ();

	for (node = ITER_NODE (iter);
	     node != tree_model->root && node != NULL;
	     node = node->parent)
		gtk_tree_path_prepend_index (
			path, g_node_child_position (node->parent, node));
	g_assert (node != NULL);

	gtk_tree_path_prepend_index (path, 0);

	{
		GtkTreeIter roundtrip;
		g_assert (accessible_tree_model_get_iter (model, &roundtrip, path));
		g_assert (ITER_NODE (&roundtrip) == ITER_NODE (iter));
	}

	return path;
}

static void
accessible_tree_model_get_value (GtkTreeModel *model,
				 GtkTreeIter  *iter,
				 int           column,
				 GValue       *value)
{
	ModelNode *mnode;

	mnode = ITER_NODE (iter)->data;

	switch (column) {
	case ACCESSIBLE_TREE_MODEL_NAME_COL:
		g_value_init (value, G_TYPE_STRING);

		if (!mnode->name) {
			mnode->name = Accessible_getName (mnode->accessible);
			if (!mnode->name || mnode->name [0] == '\0') {
				SPI_freeString (mnode->name);
				mnode->name = SPI_dupString ("<no name>");
			}
		}
		g_value_set_string (value, mnode->name);
		break;

	case ACCESSIBLE_TREE_MODEL_ROLE_COL:
		g_value_init (value, G_TYPE_STRING);

		if (!mnode->role_name) {
			mnode->role_name = Accessible_getRoleName (mnode->accessible);
			if (!mnode->role_name || mnode->role_name [0] == '\0') {
				SPI_freeString (mnode->role_name);
				mnode->role_name = SPI_dupString ("<no role>");
			}
		}
		g_value_set_string (value, mnode->role_name);
		break;

	case ACCESSIBLE_TREE_MODEL_DESCR_COL:
		g_value_init (value, G_TYPE_STRING);

		if (!mnode->description) {
			mnode->description = Accessible_getDescription (mnode->accessible);
			if (!mnode->description || mnode->description [0] == '\0') {
				SPI_freeString (mnode->description);
				mnode->description = SPI_dupString ("<no description>");
			}
		}
		g_value_set_string (value, mnode->description);
		break;

	case ACCESSIBLE_TREE_MODEL_ICON_COL: {
		GdkPixbuf *pixbuf;

		if (mnode->role == SPI_ROLE_INVALID)
			mnode->role = Accessible_getRole (mnode->accessible);

		g_value_init (value, GDK_TYPE_PIXBUF);

		pixbuf = get_pixbuf_for_role (mnode->role);
		g_value_set_object (value, pixbuf);

		break;
	}
	default:
		g_assert_not_reached ();
	}
}

static gboolean
accessible_tree_model_iter_next (GtkTreeModel *model,
				 GtkTreeIter  *iter)
{
	if (ITER_NODE (iter))
		return make_iter (model, ITER_NODE (iter)->next, iter);
	else
		return make_iter_invalid (iter);
}

static gboolean
accessible_tree_model_iter_children (GtkTreeModel *model,
				     GtkTreeIter  *iter,
				     GtkTreeIter  *parent_iter)
{
	if (ITER_NODE (parent_iter)) {
		model_node_sync_children (model, ITER_NODE (parent_iter));
		return make_iter (model, ITER_NODE (parent_iter)->children, iter);
	} else
		return make_iter_invalid (iter);
}

static gboolean
accessible_tree_model_iter_parent (GtkTreeModel *model,
				   GtkTreeIter  *iter,
				   GtkTreeIter  *child_iter)
{
	if (ITER_NODE (child_iter))
		return make_iter (model, ITER_NODE (child_iter)->parent, iter);
	else
		return make_iter_invalid (iter);
}

static gboolean
accessible_tree_model_iter_has_child (GtkTreeModel *model,
				      GtkTreeIter  *iter)
{
	GNode *node = ITER_NODE (iter);

	if (node) {
		return model_node_has_children (model, node);
	} else
		return FALSE;
}

static int
accessible_tree_model_iter_n_children (GtkTreeModel *model,
				       GtkTreeIter  *iter)
{
	GNode *node = ITER_NODE (iter);

	if (node) {
		model_node_sync_children (model, node);
		return g_node_n_children (node->children);
	} else
		return 0;
}

static gboolean
accessible_tree_model_iter_nth_child (GtkTreeModel *model,
				      GtkTreeIter  *iter,
				      GtkTreeIter  *parent_iter,
				      int           n)
{
	if (!parent_iter) {
		AccessibleTreeModel *tree_model;

		tree_model = ACCESSIBLE_TREE_MODEL (model);

		make_iter (model, tree_model->root, iter);
	
		return (tree_model->root != NULL);
	}

	return make_iter (model,
			  g_node_nth_child (ITER_NODE (parent_iter), n),
			  iter);
}

static void
accessible_tree_model_ref_node (GtkTreeModel *model,
				GtkTreeIter  *iter)
{
	GNode *node = ITER_NODE (iter);
	ModelNode *mnode;

	if (!node || !(mnode = node->data)) {
		g_warning ("Foo ref");
		return;
	}

	mnode->ref++;
}

static void
accessible_tree_model_unref_node (GtkTreeModel *tree_model, GtkTreeIter *iter)
{
	GNode *node = ITER_NODE (iter);
	ModelNode *mnode;
	AccessibleTreeModel *model;

	if (!node || !(mnode = node->data))
		return;

	mnode->ref--;
	if (mnode->ref == 0) {
		model = ACCESSIBLE_TREE_MODEL (tree_model);
		if (model->root == node) {
			model_node_destroy (tree_model, node);
			model->root = NULL;
		}
	}
}

gboolean
accessible_is_base_accessible (Accessible *accessible)
{
	gboolean ret;
	CORBA_Environment ev;
	CORBA_Object *a = (CORBA_Object *) accessible;

	if (!a)
		return TRUE;

	CORBA_exception_init (&ev);
	ret = CORBA_Object_is_a (*a, "IDL:Accessibility/Accessible:1.0", &ev);
	if (BONOBO_EX (&ev)) {
#ifdef JAVA_ENABLED
		/*
		* The Java ORB fails when an "is_a" invocation is sent over the
		* wire with an exception BAD_OPERATION. 
		* Because of this, we need to make an attempt at using something
		* else to determin if the base is Accessible or not.
		*/
		Accessible *aa;
		
		aa = Accessible_queryInterface (accessible, "IDL:Accessibility/Accessible:1.0");
		if ( aa != NULL ) {
		    ret = TRUE;
		    Accessible_unref (aa);
		}
		else
		    ret = FALSE;
#else /* JAVA_ENABLED */
		ret = FALSE;
#endif /* JAVA_ENABLED */
	}
	CORBA_exception_free (&ev);

	return ret;
}

GtkTreeModel *
accessible_tree_model_new (Accessible *root)
{
	GtkTreeModel *model;

	g_return_val_if_fail (root != NULL, NULL);
	
	model = g_object_new (ACCESSIBLE_TYPE_TREE_MODEL, NULL);

	g_assert (accessible_is_base_accessible (root)); 

	model_construct (ACCESSIBLE_TREE_MODEL (model), root);

	model_node_inserted (model, ACCESSIBLE_TREE_MODEL (model)->root);

	return model;
}

static void
report_global_event (const AccessibleEvent *event, void *user_data)
{
	AccessibleTreeModel *model = ACCESSIBLE_TREE_MODEL (user_data);

	if (!model || !model->accessible_to_node)
		return;

	if (!strncmp (event->type, "object:children-changed", 
                           strlen( "object:children-changed") )) {
		model_node_children_changed (model, event->source);
	} else if (!strcmp (event->type, "object:active-descendant-changed")) {
		Accessible *active_descendant;
		char *role_txt;

		active_descendant = AccessibleActiveDescendantChangedEvent_getActiveDescendant (event);

		role_txt = Accessible_getRoleName (active_descendant);

		g_print ("New active descendant: %p Role: %s Index: %ld\n",
			 active_descendant, role_txt, event->detail1);
		SPI_freeString (role_txt);
	}
}

static void
accessible_tree_model_init (AccessibleTreeModel *model)
{
	model->stamp = 1;

	model->event_listener = SPI_createAccessibleEventListener (
		report_global_event, model);
	SPI_registerGlobalEventListener (
		model->event_listener, "object:children-changed");
	SPI_registerGlobalEventListener (
		model->event_listener, "object:active-descendant-changed");
}

static void
accessible_tree_model_dispose (GObject *object)
{
	AccessibleTreeModel *model;

	model = ACCESSIBLE_TREE_MODEL (object);
	if (model->root)
		model_node_destroy (GTK_TREE_MODEL (model), model->root);

	if (model->event_listener != NULL) {
		SPI_deregisterGlobalEventListenerAll (
			model->event_listener);
		AccessibleEventListener_unref (
			model->event_listener);
		model->event_listener = NULL;
	}

	G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
accessible_tree_model_class_init (GObjectClass *gobject_class)
{
	parent_class = g_type_class_peek_parent (gobject_class);

	gobject_class->dispose = accessible_tree_model_dispose;
}

static void
accessible_tree_model_tree_model_init (GtkTreeModelIface *iface)
{
	iface->get_n_columns = accessible_tree_model_get_n_columns;
	iface->get_column_type = accessible_tree_model_get_column_type;
	iface->get_iter = accessible_tree_model_get_iter;
	iface->get_path = accessible_tree_model_get_path;
	iface->get_value = accessible_tree_model_get_value;
	iface->iter_next = accessible_tree_model_iter_next;
	iface->iter_children = accessible_tree_model_iter_children;
	iface->iter_has_child = accessible_tree_model_iter_has_child;
	iface->iter_n_children = accessible_tree_model_iter_n_children;
	iface->iter_nth_child = accessible_tree_model_iter_nth_child;
	iface->iter_parent = accessible_tree_model_iter_parent;
	iface->ref_node = accessible_tree_model_ref_node;
	iface->unref_node = accessible_tree_model_unref_node;
}

GType
accessible_tree_model_get_type (void)
{
	static GType object_type = 0;

	if (object_type == 0) {
		static const GTypeInfo object_info = {
			sizeof (AccessibleTreeModelClass),
			NULL,
			NULL,
			(GClassInitFunc) accessible_tree_model_class_init,
			NULL,
			NULL,
			sizeof (AccessibleTreeModel),
			0,
			(GInstanceInitFunc) accessible_tree_model_init,
		};

		static const GInterfaceInfo tree_model_info = {
			(GInterfaceInitFunc) accessible_tree_model_tree_model_init,
			NULL,
			NULL
		};

		object_type = g_type_register_static (
			G_TYPE_OBJECT, "AccessibleTreeModel", &object_info, 0);
		g_type_add_interface_static (object_type,
					     GTK_TYPE_TREE_MODEL,
					     &tree_model_info);
	}

	return object_type;
}

Accessible *
accessible_tree_get_from_iter (GtkTreeIter *iter)
{
	ModelNode *mnode;

	g_return_val_if_fail (iter != NULL, NULL);
	g_return_val_if_fail (ITER_NODE (iter) != NULL, NULL);
	
	mnode = ITER_NODE (iter)->data;

	return mnode->accessible;
}


/**
 * accessible_to_path:
 * @accessible: the accessible
 * 
 *   Convert the @accessible's location into a
 * path of sorts, try to be more robust than
 * pure integer offets by using names etc.
 * 
 * Return value: the g_allocated path, or NULL on
 * fatal exception
 **/
char *
accessible_get_path (Accessible *accessible)
{
	GString *path;
	gboolean error = FALSE;
	GSList  *l, *parents = NULL;

	while (accessible) {
		parents = g_slist_prepend (parents, accessible);
		accessible = Accessible_getParent (accessible);
	}

	path = g_string_new ("");

	for (l = parents; l; l = l->next) {
		long  idx;
		char *name, *role;
		Accessible *a = l->data;
		AccessibleRole r;

		g_string_append_c (path, '/');

		idx = Accessible_getIndexInParent (a);
		g_string_append_printf (path, "%ld", idx);

		name = Accessible_getName (a);
		if (name)
			g_string_append (path, name);
		else {
			error = TRUE;
			goto out;
		}
		SPI_freeString (name);
		g_string_append_c (path, ',');

		role = Accessible_getRoleName (a);
		if (role)
			g_string_append (path, role);
		else {
			error = TRUE;
			goto out;
		}
		SPI_freeString (role);
		g_string_append_c (path, ',');

		r = Accessible_getRole (a);
		g_string_append_printf (path, "%d", r);
	}

 out:
	for (l = parents; l; l = l->next)
		Accessible_unref (l->data);
	g_slist_free (parents);

	return g_string_free (path, error);
}

/**
 * accessible_from_path:
 * @path: the path
 * 
 * retreive the accessible with the given path
 * 
 * Return value: an accessible with that path.
 **/
Accessible *
accessible_from_path (const char *path)
{
	g_warning (G_STRLOC "implement me");
	return NULL;
}

gboolean
accessible_tree_owns_node (AccessibleTreeModel *model,
			   Accessible          *accessible)
{
	return g_hash_table_lookup (model->accessible_to_node, accessible) != NULL;
}

char *
accessible_tree_model_path (AccessibleTreeModel *model,
			    Accessible          *accessible)
{
	char *str;
	GNode *node;
	GtkTreeIter iter;
	GtkTreePath *path;
	GtkTreeModel *gtk_model;

	gtk_model = GTK_TREE_MODEL (model);

	if (!(node = g_hash_table_lookup (model->accessible_to_node, accessible))) {
		g_warning ("We don't own this node");
		return NULL;
	}

	make_iter (gtk_model, node, &iter);

	path = gtk_tree_model_get_path (gtk_model, &iter);
	g_return_val_if_fail (path != NULL, NULL);

	str = gtk_tree_path_to_string (path);

	gtk_tree_path_free (path);

	return str;
}
