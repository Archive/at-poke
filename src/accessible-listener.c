/*
 * selection-listener.c: listens to selected at-spi events
 *                       and calls into the at-poke API.
 *
 * Author:
 *    Bill Haneman
 *
 * Copyright 2002 Sun Microsystems, Inc.
 */
#include <string.h>
#include "accessible-listener.h"
#include <libgnome/gnome-macros.h>

enum {
	UPDATE,
	UPDATE_TEXT,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL];
static GObjectClass *parent_class;
static gchar *event_names[8] = {
	"object:text-changed",
	"object:text-caret-moved",
	"object:text-selection-changed",
	"object:selection-changed",
	"object:property-change",
	"object:visible-data-changed",
	"object:state-changed",
	NULL
};

static AccessibleListener *listener = NULL;

GType accessible_listener_get_type (void);

GNOME_CLASS_BOILERPLATE (AccessibleListener,
			 accessible_listener,
			 GObject,
			 G_TYPE_OBJECT)

static void
accessible_listener_global_event (const AccessibleEvent *event,
				  void                  *user_data)
{
	AccessibleListener *al = user_data;
	if (event->source == al->target)
		g_signal_emit (al, signals [UPDATE], 0);
}

static void
accessible_listener_global_text_event (const AccessibleEvent *event,
				       void                  *user_data)
{
	AccessibleListener *al = user_data;
	if (event->source == al->target)
		g_signal_emit (al, signals [UPDATE_TEXT], 0);
}

static void
accessible_listener_instance_init (AccessibleListener *al)
{
	gint i;
	al->el = SPI_createAccessibleEventListener (
		accessible_listener_global_event, al);

	al->tel = SPI_createAccessibleEventListener (
		accessible_listener_global_text_event, al);
	
	for (i = 0; event_names[i]; ++i) {
		if (strstr (event_names[i], "text"))
			SPI_registerGlobalEventListener (al->tel,
							 event_names[i]);
		else
			SPI_registerGlobalEventListener (al->el,
							 event_names[i]);
	}
	/* TODO: listen to various table events also */
}

static void
accessible_listener_dispose (GObject *object)
{
	AccessibleListener *al = (AccessibleListener *) object;

	if (al->el) {
		SPI_deregisterGlobalEventListenerAll (al->el);
		AccessibleEventListener_removeCallback (al->el,
							accessible_listener_global_event);
		AccessibleEventListener_unref (al->el);
		al->el = NULL;
	}
	if (al->tel) {
		SPI_deregisterGlobalEventListenerAll (al->tel);
		AccessibleEventListener_removeCallback (al->el,
							accessible_listener_global_text_event);
		AccessibleEventListener_unref (al->tel);
		al->tel = NULL;
	}

	parent_class->dispose (object);
}

static void
accessible_listener_class_init (AccessibleListenerClass *klass)
{
	GObjectClass *gobject_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	gobject_class->dispose = accessible_listener_dispose;
	
	signals [UPDATE] =
		g_signal_new ("object-update",
			      G_TYPE_FROM_CLASS (gobject_class),
			      G_SIGNAL_RUN_LAST,
			      0,
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	signals [UPDATE_TEXT] =
		g_signal_new ("text-update",
			      G_TYPE_FROM_CLASS (gobject_class),
			      G_SIGNAL_RUN_LAST,
			      0,
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
}


AccessibleListener *
accessible_listener_get (void)
{
	if (!listener)
		listener = g_object_new (
			accessible_listener_get_type (), NULL);

	return listener;
}

void
accessible_listener_set_target (AccessibleListener *al,
				Accessible *accessible)
{
	al->target = accessible;
}

void
accessible_listener_shutdown (void)
{
	if (listener) {
		g_object_unref (listener);
		listener = NULL;
	}	
}
