/*
 * accessible-listener.h: listens to at-spi events and
 *                       emits a signal if their source matches
 *                       the currently selected target.
 *
 * Author:
 *    Bill Haneman
 *
 * Copyright 2002 Sun Microsystems, Inc.
 */
#ifndef ACCESSIBLE_LISTENER_H
#define ACCESSIBLE_LISTENER_H

#include <cspi/spi.h>
#include <glib-object.h>

typedef struct {
	GObject parent;
	Accessible *target;
	AccessibleEventListener *el;
	AccessibleEventListener *tel;
} AccessibleListener;

typedef struct {
	GObjectClass parent_class;

	/* signals */
	void (*update) (AccessibleListener *listener, void *user_data);
} AccessibleListenerClass;

AccessibleListener *accessible_listener_get         (void);
void accessible_listener_set_target (AccessibleListener *al,
				     Accessible *accessible);
void accessible_listener_shutdown   (void);

#endif /* ACCESSIBLE_LISTENER_H */
