/*
 * main.c: the main program / app list view
 *
 * Author:
 *    Michael Meeks
 *
 * Copyright 2005 Novell, Inc.
 */
#include <config.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <cspi/spi.h>
#include <stdarg.h>
#include <string.h>

#include "graphics.h"
#include "eventlog.h"
#include "poke.h"

extern void poke (Accessible *accessible);

typedef enum {
	FILTER_COLUMN_NAME,
	FILTER_COLUMN_ENABLED,
	FILTER_COLUMN_INCONSISTENT,
	FILTER_COLUMN_FULL_NAME,
	FILTER_COLUMN_LAST
} FilterColumns;

struct _EventLog {
	GladeXML      *xml;
	GtkWidget     *window;
	GtkTextView   *text_view;
	GtkTreeView   *filters_view;

	GtkTextBuffer *log_text;
	GtkTreeStore  *filters;

	AccessibleEventListener *global_listener;

	guint          idle_tail;

#ifdef IDLE_LOGGING
	GList         *event_list;
	guint          idle_log;
#endif
};

static gboolean
log_do_tail (EventLog *log)
{
	GtkTextIter iter;

	gtk_text_buffer_get_end_iter (log->log_text, &iter);
	gtk_text_view_scroll_to_iter (log->text_view, &iter,
				      FALSE, FALSE, 0.0, 1.0);
	log->idle_tail = 0;

	return FALSE;
}

static void
log_track_end (EventLog *log)
{
	if (log->idle_tail)
		return;
	log->idle_tail = g_idle_add ((GSourceFunc) log_do_tail, log);
}

static char *
ellipsize (const char *str)
{
	char *truncated;
	char *result;

	if (!str)
		return NULL;

	if (strlen (str) < 16)
		return g_strdup (str);

	truncated = g_strndup (str, 13);
	result = g_strconcat (truncated, "...", NULL);
	g_free (truncated);

	return result;
}

static void
log_do_append_text (EventLog *log, GtkTextTag *tag,
		    const char *format, va_list args)
{	
	GtkTextIter iter;
	char *msg;
	
	msg = g_strdup_vprintf (format, args);
	
	gtk_text_buffer_get_end_iter (log->log_text, &iter);
	gtk_text_buffer_insert_with_tags (log->log_text, &iter,
					  msg, strlen (msg), tag, NULL);

	g_free (msg);

	log_track_end (log);

}

static void
log_message (EventLog *log, const char *format, ...)
{
	va_list args;
	va_start (args, format);
	log_do_append_text (log, NULL, format, args);
	va_end (args);
}

static void
log_error (EventLog *log, gboolean prepend_newline, const char *format, ...)
{
	GtkTextTag *tag;

	if (prepend_newline)
		log_message (log, "\n");

	va_list args;
	va_start (args, format);

	tag = gtk_text_buffer_create_tag (log->log_text, NULL, "foreground", "red", NULL);
	log_do_append_text (log, tag, format, args);
	g_object_unref (tag);
	va_end (args);
}

static void
log_accessible (EventLog *log, Accessible *accessible)
{
	GtkTextTag *tag;
	GtkTextIter iter;
	char *text, *name, *descr, *short_descr;
	char *role_name, *ifaces;

	if (!accessible) {
		log_message (log, "<Null>");
		return;
	}

	tag = gtk_text_buffer_create_tag (log->log_text, NULL, 
					  "foreground", "blue", 
					  "underline", PANGO_UNDERLINE_SINGLE, 
					  NULL);
	Accessible_ref (accessible);
	g_object_set_data_full (G_OBJECT (tag), "accessible", accessible,
				(GDestroyNotify) Accessible_unref );

	ifaces = accessible_get_iface_string (accessible);
	role_name = Accessible_getRoleName (accessible);
	name = Accessible_getName (accessible);
	descr = Accessible_getDescription (accessible);
	short_descr = ellipsize (descr);

	/* FIXME: nice mangled printout of supported interfaces ? */
	text = g_strdup_printf ("%s:%s:%s:%s",
				ifaces,
				role_name ? role_name : "--",
				name ? name : "--",
				short_descr ? short_descr : "--");

	gtk_text_buffer_get_end_iter (log->log_text, &iter);
	gtk_text_buffer_insert_with_tags (log->log_text, &iter, text, -1, tag, NULL);

	g_free (text);
	g_free (short_descr);
	SPI_freeString (descr);
	SPI_freeString (name);
	SPI_freeString (role_name);
	g_free (ifaces);

	log_track_end (log);
}

typedef enum {
	SANITY_NOCHECK_SOURCE,
	SANITY_CHECK_SOURCE
} SanityCheckType;

static gboolean
log_sanity_check (EventLog *log, const AccessibleEvent *event, SanityCheckType check_source)
{
	if (!event) {
		log_error (log, FALSE, "event is NULL\n");
		return FALSE;
	}
	if (!event->type) {
		log_error (log, FALSE, "event has no type coming from accessible");
		log_accessible (log, event->source);
		log_error (log, FALSE, "\n");
		return FALSE;
	}
	if (check_source == SANITY_CHECK_SOURCE && !event->source) {
		log_error (log, FALSE, "event '%s' has no source\n", event->type);
		return FALSE;
	}
	return TRUE;
}

static void
log_generic (EventLog *log, const AccessibleEvent *event)
{
	if (!log_sanity_check (log, event, SANITY_NOCHECK_SOURCE))
		return;
	log_message (log, "generic event '%s' ", event->type);
	log_accessible (log, event->source);
	log_message (log, "(%ld) (%ld)\n", event->detail1, event->detail2);
}

static gboolean
log_sanity_check_text (EventLog *log, const AccessibleEvent *event)
{
	if (!log_sanity_check (log, event, SANITY_CHECK_SOURCE))
		return FALSE;

	if (!Accessible_isText (event->source)) {
		log_error (log, TRUE, "caret moved event on object not supporting the text interface\n");
		return FALSE;
	}
	return TRUE;
}

static void
log_object_text_caret_moved (EventLog *log, const AccessibleEvent *event)
{
	AccessibleText *text;
	long int start, end, offset, text_length;

	if (!log_sanity_check_text (log, event))
		return;

	offset = event->detail1;

	log_message (log, "caret moved (%s) to %d on ", event->type, offset);
	log_accessible (log, event->source);

	text = Accessible_getText (event->source);

	offset = event->detail1;
	text_length = AccessibleText_getCharacterCount (text);
	if (offset < 0 || offset > text_length) {
		log_error (log, TRUE, "caret moved beyond end of text %d characters long", text_length);
		return;
	}

	AccessibleText_getTextAtOffset (text, offset, 
					SPI_TEXT_BOUNDARY_SENTENCE_START,
					&start, &end);
	log_message (log, "offsets: sentance_start %d->%d, ", start, end);
	AccessibleText_getTextAtOffset (text, offset, 
					SPI_TEXT_BOUNDARY_SENTENCE_END,
					&start, &end);
	log_message (log, "sentance_end %d->%d, ", start, end);
	AccessibleText_getTextAtOffset (text, offset, 
					SPI_TEXT_BOUNDARY_LINE_START,
					&start, &end);
	log_message (log, "line_start %d->%d, ", start, end);
	AccessibleText_getTextAtOffset (text, offset, 
					SPI_TEXT_BOUNDARY_LINE_END,
					&start, &end);
	log_message (log, "line_end %d->%d, ", start, end);

	AccessibleText_getTextAtOffset (text, offset, 
					SPI_TEXT_BOUNDARY_WORD_START,
					&start, &end);
	log_message (log, "word_start %d->%d, ", start, end);
	AccessibleText_getTextAtOffset (text, offset, 
					SPI_TEXT_BOUNDARY_WORD_END,
					&start, &end);
	log_message (log, "word_end %d->%d, ", start, end);
	log_message (log, "\n");
}

void
log_object_text_changed (EventLog *log, const AccessibleEvent *event)
{
	char *text;
	if (!log_sanity_check_text (log, event))
		return;

	log_message (log, "text changed (%s) offset %d length %d on ",
		     event->type, event->detail1, event->detail2);
	log_accessible (log, event->source);

	text = AccessibleTextChangedEvent_getChangeString (event);
	log_message (log, " change string '%s'", text ? text : "<null>");
	SPI_freeString (text);
}

void
log_object_text_selection_changed (EventLog *log, const AccessibleEvent *event)
{
	char *text;
	if (!log_sanity_check_text (log, event))
		return;

	/* FIXME: more detail on the details */
	log_message (log, "text selection changed (%s) %d %d on ",
		     event->type, event->detail1, event->detail2);
	log_accessible (log, event->source);

	text = AccessibleTextSelectionChangedEvent_getSelectionString (event);
	log_message (log, "context '%s'\n", text);
	SPI_freeString (text);
}

void
log_object_active_descendant_changed (EventLog *log, const AccessibleEvent *event)
{
	Accessible *desc;

	if (!log_sanity_check (log, event, SANITY_CHECK_SOURCE) )
		return;

	log_message (log, "active descendant changed %d %d parent: ",
		     event->detail1, event->detail2);
	log_accessible (log, event->source);
	log_message (log, " child: ");
	desc = AccessibleActiveDescendantChangedEvent_getActiveDescendant (event);
	log_accessible (log, desc);
	log_message (log, "\n");
	
	Accessible_unref (desc);
}

void
log_object_children_changed (EventLog *log, const AccessibleEvent *event)
{
	Accessible *child;

	if (!log_sanity_check (log, event, SANITY_CHECK_SOURCE) )
		return;

	log_message (log, "children changed event (%s) %d %d parent: ", event->type,
		     event->detail1, event->detail2);
	log_accessible (log, event->source);

	child = AccessibleChildChangedEvent_getChildAccessible (event);
	log_message (log, " child: ");
	log_accessible (log, child);
	log_message (log, "\n");

	Accessible_unref (child);
}

void
log_object_link_selected (EventLog *log, const AccessibleEvent *event)
{
	if (!log_sanity_check (log, event, SANITY_CHECK_SOURCE) )
		return;
	
	log_message (log, "event (%d) link %d selected on ", event->type,
		     event->detail1 );
	log_accessible (log, event->source);
	log_message (log, "\n");
}

void
log_string_prop_change (EventLog *log, const AccessibleEvent *event,
			const char *prop, const char *to)
{
	if (!log_sanity_check (log, event, SANITY_CHECK_SOURCE) )
		return;

	log_message (log, "property event change %s to '%s' on ", prop, to);
	log_accessible (log, event->source);
	log_message (log, "\n");
}

void
log_object_property_change (EventLog *log, const AccessibleEvent *event)
{
	char *detail;

	if (!log_sanity_check (log, event, SANITY_NOCHECK_SOURCE) )
		return;

	{
		char **split = g_strsplit (event->type, ":", -1);
		if (split && split[0] && split[1] && split[2])
			detail = g_strdup (split[2]);
		else
			detail = g_strdup ("");
		g_strfreev (split);
	}

	if (!strcmp (detail, "accessible-name")) {
		char *text = AccessibleNameChangedEvent_getNameString (event);
		log_string_prop_change (log, event, "name", text);
		SPI_freeString (text);

	} else if (!strcmp (detail, "accessible-description")) {
		char *text = AccessibleDescriptionChangedEvent_getDescriptionString (event);
		log_string_prop_change (log, event, "description", text);
		SPI_freeString (text);

	} else if (!strcmp (detail, "accessible-parent")) {
		Accessible *parent = AccessibleParentChangedEvent_getParentAccessible (event);
		log_message (log, "parent change %d %d on ", event->detail1, event->detail2);
		log_accessible (log, event->source);
		log_message (log, " to parent: ");
		log_accessible (log, parent);
		log_message (log, "\n");
		
		Accessible_unref (parent);
 	} else {
		log_message (log, "generic property change (%s) %d %d on ",
			     event->type, event->detail1, event->detail2);
		log_accessible (log, event->source);
		log_message (log, "\n");
	}
	g_free (detail);
}

void
log_mouse (EventLog *log, const AccessibleEvent *event)
{
	if (!log_sanity_check (log, event, SANITY_NOCHECK_SOURCE) )
		return;

	log_message (log, "mouse event '%s' (%d) (%d) on ",
		     event->type, event->detail1, event->detail2);
	log_accessible (log, event->source);
	log_message (log, "\n");
}

void
log_mouse_button (EventLog *log, const AccessibleEvent *event)
{
	int button_number = -1;
	char pressed = '\0';
	const char *type = event->type + strlen ("mouse:button");

	if (!log_sanity_check (log, event, SANITY_CHECK_SOURCE) )
		return;

	sscanf (type, ":%d%c", &button_number, &pressed);
	log_message (log, "mouse button %d %s event x: %d y: %d\n",
		     button_number,
		     pressed == '\0' ? "<unknown>" : pressed == 'p' ?
		     "press" : "release",
		     event->detail1, event->detail2);
	/* FIXME: assert source == desktop */
}

void
log_keyboard_modifiers (EventLog *log, const AccessibleEvent *event)
{
	if (!log_sanity_check (log, event, SANITY_CHECK_SOURCE) )
		return;

	log_message (log, "keyboard modifier event masks: old 0x%x new 0x%x\n",
		     event->detail1, event->detail2);
	/* FIXME: assert source == desktop */
}

void
log_window (EventLog *log, const AccessibleEvent *event)
{
	char *title;

	if (!log_sanity_check (log, event, SANITY_CHECK_SOURCE) )
		return;

	log_message (log, "window event '%s' (%d) (%d) on ",
		     event->type, event->detail1, event->detail2);
	log_accessible (log, event->source);

	title = AccessibleWindowEvent_getTitleString (event);
	log_message (log, " with title '%s'\n", title ? title : "<Null>");
	SPI_freeString (title);
}

static void
events_save_as_clicked (GtkButton *button, EventLog *log)
{
	GtkWidget *file_chooser;

	file_chooser = gtk_file_chooser_dialog_new
		("Save log as", GTK_WINDOW (log->window),
		 GTK_FILE_CHOOSER_ACTION_SAVE,
		 GTK_STOCK_OK, GTK_RESPONSE_OK,
		 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		 NULL);
	if (gtk_dialog_run (GTK_DIALOG (file_chooser)) == GTK_RESPONSE_OK )
	{
	    gchar *fname = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_chooser));
	    FILE *log_file = fopen (fname, "w+");
	    if (!log_file) {
		    GtkWidget *msg = gtk_message_dialog_new (GTK_WINDOW (log->window),
							     GTK_DIALOG_MODAL,
							     GTK_MESSAGE_ERROR,
							     GTK_BUTTONS_CLOSE,
							     "Failed to open '%s' for writing",
							     fname);
		    gtk_dialog_run (GTK_DIALOG (msg));
		    gtk_widget_destroy (msg);
	    } else {
		    gchar *whole_buffer;
		    GtkTextIter start, end;

		    gtk_text_buffer_get_start_iter (log->log_text, &start);
		    gtk_text_buffer_get_end_iter (log->log_text, &end);

		    whole_buffer = gtk_text_buffer_get_text (log->log_text, &start, &end, FALSE);
		    fwrite (whole_buffer, strlen (whole_buffer), 1, log_file);
		    
		    fclose (log_file);
	    }
		    
	    g_free (fname);
	}

	gtk_widget_destroy (file_chooser);
}

static void
events_clear_clicked (GtkButton *button, EventLog *log)
{
	gtk_text_buffer_set_text (log->log_text, "", 0);
}

static gboolean
link_clicked (GtkWidget      *widget,
	      GdkEventButton *event,
	      EventLog       *log)
{
	int x, y;
	GtkTextIter iter;
	GSList *tags, *l;

	if (event->button != 1 ||
	    event->type != GDK_2BUTTON_PRESS)
		return FALSE;

	gtk_text_view_window_to_buffer_coords (GTK_TEXT_VIEW (log->text_view), 
					       GTK_TEXT_WINDOW_WIDGET,
					       event->x, event->y, &x, &y);

	gtk_text_view_get_iter_at_location (GTK_TEXT_VIEW (log->text_view), &iter, x, y);

	for (l = tags = gtk_text_iter_get_tags (&iter); l; l = l->next) {
		Accessible *accessible;

		if ((accessible = g_object_get_data (l->data, "accessible")))
		{
			poke (accessible);
			return TRUE;
		}
	}
	g_slist_free (tags);

	return FALSE;
}

typedef void (*EventLogFn) (EventLog *, const AccessibleEvent *);
typedef enum {
	EVENT_TYPE_FOCUS,
	EVENT_TYPE_MOUSE,
	EVENT_TYPE_OBJECT,
	EVENT_TYPE_WINDOW,
	EVENT_TYPE_KEYBOARD,
	EVENT_TYPE_TOOLKIT,
	EVENT_TYPE_LAST
} EventType;
static const char *event_type_names[EVENT_TYPE_LAST] =
	{ "focus", "mouse", "object", "window",
	  "keyboard", "toolkit" };
static EventLogFn event_base_log[EVENT_TYPE_LAST] =
	{ NULL, /* focus */
	  log_mouse, /* mouse */
	  NULL, /* object */
	  log_window, /* window */
	  NULL, /* keyboard */
	  NULL /* toolkit */ };

static struct {
	EventType   type;
	const char *name;
	EventLogFn  log_fn;
} event_info[] = {
	{ EVENT_TYPE_FOCUS,    NULL, NULL },

    	{ EVENT_TYPE_MOUSE,    "abs", NULL },
    	{ EVENT_TYPE_MOUSE,    "button", log_mouse_button },
    	{ EVENT_TYPE_MOUSE,    "rel", NULL },

	{ EVENT_TYPE_OBJECT,   "active-descendant-changed", log_object_active_descendant_changed },
	{ EVENT_TYPE_OBJECT,   "bounds-changed", NULL },
	{ EVENT_TYPE_OBJECT,   "children-changed", log_object_children_changed },
	{ EVENT_TYPE_OBJECT,   "column-deleted", NULL },
	{ EVENT_TYPE_OBJECT,   "column-inserted", NULL },
	{ EVENT_TYPE_OBJECT,   "column-reordered", NULL },
	{ EVENT_TYPE_OBJECT,   "link-selected", log_object_link_selected },
	{ EVENT_TYPE_OBJECT,   "model-changed", NULL },
	{ EVENT_TYPE_OBJECT,   "property-change", log_object_property_change },
	{ EVENT_TYPE_OBJECT,   "row-deleted", NULL },
	{ EVENT_TYPE_OBJECT,   "row-inserted", NULL },
	{ EVENT_TYPE_OBJECT,   "row-reordered", NULL },
	{ EVENT_TYPE_OBJECT,   "selection-changed", NULL },
	{ EVENT_TYPE_OBJECT,   "state-changed", NULL },
	{ EVENT_TYPE_OBJECT,   "test", NULL },
	{ EVENT_TYPE_OBJECT,   "text-caret-moved", log_object_text_caret_moved },
	{ EVENT_TYPE_OBJECT,   "text-changed", log_object_text_changed },
	{ EVENT_TYPE_OBJECT,   "text-selection-changed", log_object_text_selection_changed },
	{ EVENT_TYPE_OBJECT,   "visible-data-changed", NULL },

    	{ EVENT_TYPE_WINDOW,   "activate", NULL },
    	{ EVENT_TYPE_WINDOW,   "deactivate", NULL },
    	{ EVENT_TYPE_WINDOW,   "desktop-create", NULL },
    	{ EVENT_TYPE_WINDOW,   "desktop-destroy", NULL },
    	{ EVENT_TYPE_WINDOW,   "close", NULL },
    	{ EVENT_TYPE_WINDOW,   "create", NULL },
    	{ EVENT_TYPE_WINDOW,   "destroy", NULL },
    	{ EVENT_TYPE_WINDOW,   "lower", NULL },
    	{ EVENT_TYPE_WINDOW,   "minimize", NULL },
    	{ EVENT_TYPE_WINDOW,   "maximize", NULL },
    	{ EVENT_TYPE_WINDOW,   "raise", NULL },
    	{ EVENT_TYPE_WINDOW,   "restore", NULL },
    	{ EVENT_TYPE_WINDOW,   "restyle", NULL },
    	{ EVENT_TYPE_WINDOW,   "shade", NULL },
    	{ EVENT_TYPE_WINDOW,   "unshade", NULL },

    	{ EVENT_TYPE_KEYBOARD, "modifiers", log_keyboard_modifiers },

	{ EVENT_TYPE_TOOLKIT,  NULL, NULL },
};

static void
log_event (EventLog *log, const AccessibleEvent *event)
{
	int i;
	EventType etype;
	EventLogFn log_fn = NULL;
	char **split = g_strsplit (event->type, ":", -1);

	etype = EVENT_TYPE_LAST;
	if (split && split[0]) {
		for (etype = 0; etype < EVENT_TYPE_LAST; etype++) {
			if (!strcmp (split[0], event_type_names[etype]))
				break;
		}
	}

	if (etype != EVENT_TYPE_LAST && split[1]) {
		for (i = 0; i < G_N_ELEMENTS (event_info); i++ ) {
			if (event_info[i].type == etype &&
			    event_info[i].name &&
			    !strcmp (event_info[i].name, split[1])) {
				log_fn = event_info[i].log_fn;
				break;
			}
		}
	}

	if (!log_fn && etype != EVENT_TYPE_LAST)
		log_fn = event_base_log[etype];

	if (!log_fn)
		log_fn = log_generic;

	log_fn (log, event);

	g_strfreev (split);
}

#ifdef IDLE_LOGGING
static gboolean
idle_do_log (EventLog *log)
{
	GList *l, *events;

	events = log->event_list;
	log->event_list = NULL;

	for (l = events; l; l = l->next) {
		AccessibleEvent *event = l->data;
		log_event (log, event);
		AccessibleEvent_unref (event);
	}
	g_list_free (events);

	log->idle_log = 0;
	
	return FALSE;
}
#endif

static void
global_event_callback (const AccessibleEvent     *event,
		       void                      *user_data)
{
	EventLog *log = user_data;

	if (event == NULL) {
		log_error (log, FALSE, "Null event arrived");
		return;
	}
	g_return_if_fail (event != NULL);
#ifdef IDLE_LOGGING
	AccessibleEvent_ref (event);
	log->event_list = g_list_append (log->event_list, (gpointer) event);
	if (!log->idle_log)
		log->idle_log = g_idle_add ((GSourceFunc) idle_do_log, log);
#else
	log_event (log, event);
#endif
}

static gboolean
get_row_state (GtkTreeModel *model, GtkTreeIter *iter)
{
	GValue value = { 0, };
	gtk_tree_model_get_value (model, iter, FILTER_COLUMN_ENABLED, &value);
	return g_value_get_boolean (&value);
}

static gboolean
check_children (GtkTreeModel *model, GtkTreeIter *iter,
		gboolean      enabled)
{
	GtkTreeIter child;

	if (gtk_tree_model_iter_children (model, &child, iter)) {
		do {
			if (!check_children (model, &child, enabled))
				return FALSE;
		} while (gtk_tree_model_iter_next (model, &child));

	} else if (get_row_state (model, iter) != enabled)
		return FALSE;

	return TRUE;
}

static void
toggle_row (GtkTreeModel *model, GtkTreeIter *iter)
{
	GValue value = { 0, };
	GtkTreeIter parent, cur;
	gboolean enabled = get_row_state (model, iter);

	g_value_init (&value, G_TYPE_BOOLEAN);
	g_value_set_boolean (&value, !enabled);
	gtk_tree_store_set_value (GTK_TREE_STORE (model),
				  iter, FILTER_COLUMN_ENABLED, &value);

	/* re-write parents all the way up */
	cur = *iter;
	while (gtk_tree_model_iter_parent (model, &parent, &cur)) {
		gboolean inconsistent = !check_children (model, &parent, !enabled);
		g_value_set_boolean (&value, inconsistent);
		gtk_tree_store_set_value (GTK_TREE_STORE (model),
					  &parent, FILTER_COLUMN_INCONSISTENT, &value);
		if (!inconsistent) {
			g_value_set_boolean (&value, !enabled);
			gtk_tree_store_set_value (GTK_TREE_STORE (model),
						  &parent, FILTER_COLUMN_ENABLED, &value);
		}

		cur = parent;
	}
}

static void
clobber_with_children (GtkTreeModel *model, GtkTreeIter *iter,
		       gboolean new_state)
{
	GtkTreeIter child;

	if (iter)
		if (get_row_state (model, iter) != new_state)
			toggle_row (model, iter);

	if (gtk_tree_model_iter_children (model, &child, iter)) {
		do {
			clobber_with_children (model, &child, new_state);
		} while (gtk_tree_model_iter_next (model, &child));
	}
}

static void
events_select_all_clicked (GtkButton *button, EventLog *log)
{
	clobber_with_children (GTK_TREE_MODEL (log->filters), NULL, TRUE);
}

static void
update_filter_model (GtkCellRendererToggle *cell_renderer_toggle,
		     const gchar           *path,
		     EventLog              *log)
{
	GtkTreeIter iter;
	GtkTreeModel *model = GTK_TREE_MODEL (log->filters);

	if (!gtk_tree_model_get_iter_from_string (model, &iter, path))
		return;

	if (gtk_tree_model_iter_has_child (model, &iter))
		clobber_with_children (model, &iter, !get_row_state (model, &iter));
	else
		toggle_row (model, &iter);
}

static void
update_listener_row_changed (GtkTreeModel *model,
			     GtkTreePath  *path,
			     GtkTreeIter  *iter,
			     EventLog     *log)
{
	gboolean enabled = get_row_state (model, iter);
	if (!gtk_tree_model_iter_has_child (model, iter)) {
		GValue value = { 0, };
		const char *full_name;

		gtk_tree_model_get_value (model, iter,
					  FILTER_COLUMN_FULL_NAME, &value);

		full_name = g_value_get_string (&value);
		if (enabled)
			SPI_registerGlobalEventListener (log->global_listener,
							 full_name);
		else
			SPI_deregisterGlobalEventListener (log->global_listener,
							   full_name);
		g_value_unset (&value);
	}
}

static void
setup_filters (EventLog *log)
{
	int i;
	EventType   last_toplevel_type = EVENT_TYPE_LAST;
	GtkTreeIter cur_toplevel;
	GtkCellRenderer   *cell;
	GtkTreeViewColumn *column;

	log->filters = gtk_tree_store_new(FILTER_COLUMN_LAST,
					  G_TYPE_STRING, G_TYPE_BOOLEAN,
					  G_TYPE_BOOLEAN, G_TYPE_STRING);
	g_signal_connect (log->filters, "row_changed",
			  G_CALLBACK (update_listener_row_changed), log);
	
	for (i = 0; i < G_N_ELEMENTS (event_info); i++) {
		GtkTreeIter item;
		EventType type = event_info[i].type;
		const char *event_type_name = event_type_names[type];

		if (type != last_toplevel_type) {
			char *event_type_name_colon = g_strconcat (event_type_name, ":", NULL);
			last_toplevel_type = type;

			gtk_tree_store_append (log->filters, &cur_toplevel, NULL);
			gtk_tree_store_set (log->filters, &cur_toplevel,
					    FILTER_COLUMN_NAME, event_type_name,
					    FILTER_COLUMN_ENABLED, (gboolean) FALSE,
					    FILTER_COLUMN_INCONSISTENT, (gboolean) FALSE,
					    FILTER_COLUMN_FULL_NAME, event_type_name_colon,
					    -1);
			g_free (event_type_name_colon);
		}
		if (event_info[i].name) {
			char *full_name;

			gtk_tree_store_append (log->filters, &item, &cur_toplevel);
			full_name = g_strconcat (event_type_name, ":", event_info[i].name, NULL);
			gtk_tree_store_set (log->filters, &item,
					    FILTER_COLUMN_NAME, event_info[i].name,
					    FILTER_COLUMN_ENABLED, (gboolean) FALSE,
					    FILTER_COLUMN_INCONSISTENT, (gboolean) FALSE,
					    FILTER_COLUMN_FULL_NAME, full_name,
					    -1);
			g_free (full_name);
		}
	}


	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_reorderable (column, TRUE);
	gtk_tree_view_column_set_title (column, "Name");

	cell = gtk_cell_renderer_toggle_new ();
	gtk_tree_view_column_pack_start (column, cell, FALSE);
	gtk_tree_view_column_set_attributes
		(column, cell, "active", FILTER_COLUMN_ENABLED,
		 "inconsistent", FILTER_COLUMN_INCONSISTENT, NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	g_signal_connect (cell, "toggled", G_CALLBACK (update_filter_model), log);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, cell, TRUE);
	gtk_tree_view_column_set_attributes
		(column, cell, "text", FILTER_COLUMN_NAME, NULL);
	gtk_tree_view_column_set_sort_column_id (column, FILTER_COLUMN_NAME);
	gtk_tree_view_append_column (log->filters_view, column);

	gtk_tree_view_insert_column_with_attributes
		(log->filters_view, 2, "Full name",
		 gtk_cell_renderer_text_new (), "text",
		 FILTER_COLUMN_FULL_NAME, NULL);

	gtk_tree_view_set_model (log->filters_view,
				 GTK_TREE_MODEL (log->filters));
	gtk_tree_view_set_headers_visible (log->filters_view, TRUE);
	gtk_tree_selection_set_mode
		(gtk_tree_view_get_selection (log->filters_view),
		 GTK_SELECTION_NONE);
	gtk_tree_view_columns_autosize (log->filters_view);
}

EventLog *
event_log_create (GtkWindow *parent)
{
	EventLog  *log = g_new0 (EventLog, 1);
	
	log->xml = get_glade_xml();
	log->window = glade_xml_get_widget (log->xml, "event_window");

	gtk_window_set_transient_for (GTK_WINDOW (log->window), parent);
	glade_xml_signal_connect_data (log->xml, "events_clear_clicked",
				       G_CALLBACK (events_clear_clicked), log);
	glade_xml_signal_connect_data (log->xml, "events_select_all_clicked",
				       G_CALLBACK (events_select_all_clicked), log);
	glade_xml_signal_connect_data (log->xml, "events_save_as_clicked",
				       G_CALLBACK (events_save_as_clicked), log);

	log->text_view = GTK_TEXT_VIEW (glade_xml_get_widget (log->xml, "event_text_view"));
	log->filters_view = GTK_TREE_VIEW (glade_xml_get_widget (log->xml, "event_filters"));

	g_signal_connect (log->text_view, "button_press_event",
			  G_CALLBACK (link_clicked), log);

	log->log_text = gtk_text_buffer_new (NULL);
	gtk_text_view_set_buffer (log->text_view, log->log_text);
	gtk_text_view_set_wrap_mode (log->text_view, /* GTK_WRAP_NONE */ GTK_WRAP_CHAR);

	setup_filters (log);
	log->global_listener = SPI_createAccessibleEventListener (global_event_callback, log);
	log->idle_tail = 0;
#ifdef IDLE_LOGGING
	log->idle_log = 0;
	log->event_list = NULL;
#endif

	gtk_widget_show (log->window);

	return log;
}

void
event_log_destroy (EventLog *log)
{
	if (!log)
		return;

	AccessibleEventListener_removeCallback
		(log->global_listener,  global_event_callback);
	AccessibleEventListener_unref (log->global_listener);

	if (log->idle_tail > 0) {
		g_source_remove (log->idle_tail);
		log->idle_tail = 0;
	}

#ifdef IDLE_LOGGING
	if (log->idle_log > 0) {
		g_source_remove (log->idle_log);
		log->idle_log = 0;
	}
	g_list_foreach (log->event_list,
			(GFunc) AccessibleEvent_unref, NULL);
	g_list_free (log->event_list);
	log->event_list = NULL;
#endif

	gtk_widget_destroy (log->window);
	g_object_unref (log->log_text);
	g_object_unref (log->xml);

	g_free (log);
}

GtkWindow *
event_log_get_window (EventLog *log)
{
	g_return_val_if_fail (log != NULL, NULL);
	return GTK_WINDOW (log->window);
}
