/*
 * child-listener.c: tries to de-mangle the
 *                   children-changed events
 *
 * Author:
 *    Michael Meeks
 *
 * Copyright 2002 Sun Microsystems, Inc.
 */
#include "child-listener.h"
#include <libgnome/gnome-macros.h>

enum {
	APP_LIST_CHANGED,
	ROOT_DIED,
	CHILDREN_CHANGED,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL];
static GObjectClass *parent_class;
static ChildListener *listener = NULL;

GType child_listener_get_type (void);

extern Accessible *get_accessible_at_index (GtkListStore *list_store, int i);

GNOME_CLASS_BOILERPLATE (ChildListener,
			 child_listener,
			 GObject,
			 G_TYPE_OBJECT)

static void
child_listener_global_event (const AccessibleEvent *event,
			     void                  *user_data)
{
	ChildListener *cl = user_data;

	if (event->source == cl->desktop) {
		GSList *l;
		gboolean is_remove;
		Accessible *accessible = NULL;
		GSList *next;

		is_remove = g_strstr_len (event->type, -1,  "remove") != NULL;
		if (is_remove)
			accessible = get_accessible_at_index (cl->list_store, event->detail1);
		g_signal_emit (
			cl, signals [APP_LIST_CHANGED], 0);

		if (is_remove) {
			for (l = cl->roots; l; l = next) {
				next = l->next;
				if (l->data == accessible) {

					g_signal_emit (cl, signals [ROOT_DIED], 0, l->data);
					cl->roots = g_slist_delete_link (cl->roots, l);
				}
			}
		}
	} else
		g_signal_emit (cl, signals [CHILDREN_CHANGED], 0, event->source);
}

static void
child_listener_instance_init (ChildListener *cl)
{
	cl->desktop = SPI_getDesktop (0);

	cl->el = SPI_createAccessibleEventListener (
		child_listener_global_event, cl);

	SPI_registerGlobalEventListener (
		cl->el, "object:children-changed");
}

static void
child_listener_dispose (GObject *object)
{
	ChildListener *cl = (ChildListener *) object;

	if (cl->el) {
		SPI_deregisterGlobalEventListenerAll (cl->el);
		AccessibleEventListener_unref (cl->el);
		cl->el = NULL;
	}

	if (cl->desktop)
		Accessible_unref (cl->desktop);
	cl->desktop = NULL;

	parent_class->dispose (object);
}

static void
child_listener_class_init (ChildListenerClass *klass)
{
	GObjectClass *gobject_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	gobject_class->dispose = child_listener_dispose;

	signals [APP_LIST_CHANGED] =
		g_signal_new ("app_list_changed",
			      G_TYPE_FROM_CLASS (gobject_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ChildListenerClass,
					       app_list_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

	signals [ROOT_DIED] =
		g_signal_new ("root_died",
			      G_TYPE_FROM_CLASS (gobject_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ChildListenerClass,
					       root_died),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__POINTER,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);

	signals [CHILDREN_CHANGED] =
		g_signal_new ("children_changed",
			      G_TYPE_FROM_CLASS (gobject_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ChildListenerClass,
					       children_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__POINTER,
			      G_TYPE_NONE, 1, G_TYPE_POINTER);
}

ChildListener *
child_listener_create (GtkListStore *list_store)
{
	if (!listener)  {
		listener = g_object_new (
		child_listener_get_type (), NULL);
		listener->list_store = list_store;
	} else {
		g_warning ("ChildListener already created");
	}
	return listener;
}

ChildListener *
child_listener_get (void)
{
	return listener;
}

void
child_listener_add_root (ChildListener *listener, Accessible *root)
{
	listener->roots = g_slist_prepend (listener->roots, root);
}

void
child_listener_remove_root (ChildListener *listener, Accessible *root)
{
	listener->roots = g_slist_remove (listener->roots, root);
}
