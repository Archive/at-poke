#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <glade/glade-xml.h>
#include <cspi/spi-roletypes.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

GdkPixbuf *get_pixbuf_for_role (AccessibleRole role);
GladeXML  *get_glade_xml       (void);

#endif /* GRAPHICS_H */
