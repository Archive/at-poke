/*
 * graphics.c: pixmap management for accessible roles
 *
 * Authors:
 *    Michael Meeks
 *
 * Copyright 2002 Sun Microsystems, Inc.
 */

#include <libgnome/gnome-util.h>

#include "icons/button.xpm"
#include "icons/checkbutton.xpm"
#include "icons/checkmenuitem.xpm"
#include "icons/colorselection.xpm"
#include "icons/combo.xpm"
#include "icons/dialog.xpm"
#include "icons/image.xpm"
#include "icons/label.xpm"
#include "icons/menubar.xpm"
#include "icons/menuitem.xpm"
#include "icons/notebook.xpm"
#include "icons/scrolledwindow.xpm"
#include "icons/spinbutton.xpm"
#include "icons/statusbar.xpm"
#include "icons/table.xpm"
#include "icons/text.xpm"
#include "icons/toolbar.xpm"
#include "icons/tree.xpm"
#include "icons/treeitem.xpm"
#include "icons/unknown.xpm"
#include "icons/viewport.xpm"
#include "icons/vscrollbar.xpm"
#include "icons/vseparator.xpm"
#include "icons/window.xpm"

#include "graphics.h"

static const struct {
	AccessibleRole role;
	char         **xpmd;
} role_graphics [] = {
	{ SPI_ROLE_INVALID, NULL },
	{ SPI_ROLE_ACCEL_LABEL, label_xpm },
	{ SPI_ROLE_ALERT, NULL },
	{ SPI_ROLE_ANIMATION, NULL },
	{ SPI_ROLE_ARROW, NULL },
	{ SPI_ROLE_CALENDAR, NULL },
	{ SPI_ROLE_CANVAS, NULL },
	{ SPI_ROLE_CHECK_BOX, checkbutton_xpm },
	{ SPI_ROLE_CHECK_MENU_ITEM, checkmenuitem_xpm },
	{ SPI_ROLE_COLOR_CHOOSER, colorselection_xpm },
	{ SPI_ROLE_COLUMN_HEADER, NULL },
	{ SPI_ROLE_COMBO_BOX, combo_xpm },
	{ SPI_ROLE_DATE_EDITOR, NULL },
	{ SPI_ROLE_DESKTOP_ICON, NULL },
	{ SPI_ROLE_DESKTOP_FRAME, NULL },
	{ SPI_ROLE_DIAL, NULL },
	{ SPI_ROLE_DIALOG, dialog_xpm },
	{ SPI_ROLE_DIRECTORY_PANE, NULL },
	{ SPI_ROLE_DRAWING_AREA, NULL },
	{ SPI_ROLE_FILE_CHOOSER, NULL },
	{ SPI_ROLE_FILLER, NULL },
	{ SPI_ROLE_FONT_CHOOSER, NULL },
	{ SPI_ROLE_FRAME, window_xpm },
	{ SPI_ROLE_GLASS_PANE, NULL },
	{ SPI_ROLE_HTML_CONTAINER, NULL },
	{ SPI_ROLE_ICON, image_xpm },
	{ SPI_ROLE_IMAGE, image_xpm },
	{ SPI_ROLE_INTERNAL_FRAME, NULL },
	{ SPI_ROLE_LABEL, label_xpm },
	{ SPI_ROLE_LAYERED_PANE, viewport_xpm },
	{ SPI_ROLE_LIST, NULL },
	{ SPI_ROLE_LIST_ITEM, NULL },
	{ SPI_ROLE_MENU, menuitem_xpm },
	{ SPI_ROLE_MENU_BAR, menubar_xpm },
	{ SPI_ROLE_MENU_ITEM, menuitem_xpm },
	{ SPI_ROLE_OPTION_PANE, NULL },
	{ SPI_ROLE_PAGE_TAB, notebook_xpm },
	{ SPI_ROLE_PAGE_TAB_LIST, notebook_xpm },
	{ SPI_ROLE_PANEL, viewport_xpm },
	{ SPI_ROLE_PASSWORD_TEXT, NULL },
	{ SPI_ROLE_POPUP_MENU, NULL },
	{ SPI_ROLE_PROGRESS_BAR, NULL },
	{ SPI_ROLE_PUSH_BUTTON, button_xpm },
	{ SPI_ROLE_RADIO_BUTTON, NULL },
	{ SPI_ROLE_RADIO_MENU_ITEM, NULL },
	{ SPI_ROLE_ROOT_PANE, viewport_xpm },
	{ SPI_ROLE_ROW_HEADER, NULL },
	{ SPI_ROLE_SCROLL_BAR, vscrollbar_xpm },
	{ SPI_ROLE_SCROLL_PANE, scrolledwindow_xpm },
	{ SPI_ROLE_SEPARATOR, vseparator_xpm },
	{ SPI_ROLE_SLIDER, NULL },
	{ SPI_ROLE_SPIN_BUTTON, spinbutton_xpm },
	{ SPI_ROLE_SPLIT_PANE, NULL },
	{ SPI_ROLE_STATUS_BAR, statusbar_xpm },
	{ SPI_ROLE_TABLE, table_xpm },
	{ SPI_ROLE_TABLE_CELL, treeitem_xpm },
	{ SPI_ROLE_TABLE_COLUMN_HEADER, NULL },
	{ SPI_ROLE_TABLE_ROW_HEADER, NULL },
	{ SPI_ROLE_TEAROFF_MENU_ITEM, NULL },
	{ SPI_ROLE_TERMINAL, NULL },
	{ SPI_ROLE_TEXT, text_xpm },
	{ SPI_ROLE_TOGGLE_BUTTON, NULL },
	{ SPI_ROLE_TOOL_BAR, toolbar_xpm },
	{ SPI_ROLE_TOOL_TIP, NULL },
	{ SPI_ROLE_TREE, tree_xpm },
	{ SPI_ROLE_TREE_TABLE, tree_xpm },
	{ SPI_ROLE_UNKNOWN, unknown_xpm },
	{ SPI_ROLE_VIEWPORT, viewport_xpm },
	{ SPI_ROLE_WINDOW, window_xpm },
	{ SPI_ROLE_EXTENDED, NULL },
	{ 0xffffff, NULL}
};

GdkPixbuf *
get_pixbuf_for_role (AccessibleRole role)
{
	int               i;
	const char      **icon_xpm;
	GdkPixbuf        *pixbuf;
	static GdkPixbuf *pixbufs[SPI_ROLE_LAST_DEFINED] = { NULL };

	if (role < 0 || role >= SPI_ROLE_LAST_DEFINED)
		role = SPI_ROLE_INVALID;

	if (pixbufs [role])
		return pixbufs [role];

	icon_xpm = NULL;

	for (i = 0; role_graphics [i].role < 0xfffff; i++) {
		if (role_graphics[i].role == role)
			icon_xpm = (const char **) role_graphics[i].xpmd;
	}

	if (!icon_xpm)
		icon_xpm = (const char **) unknown_xpm;

	pixbuf = gdk_pixbuf_new_from_xpm_data (icon_xpm);
	pixbufs [role] = pixbuf;

	return pixbuf;
}

GladeXML *
get_glade_xml (void)
{
	GladeXML *xml = NULL;
	const char *tst;

	tst = "../glade/at-poke.glade2";
	if (g_file_test (tst, G_FILE_TEST_EXISTS))
		xml = glade_xml_new (tst, NULL, NULL);
	else {
		char *fname;
			
		fname = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_APP_DATADIR,
						   "at-poke.glade2", TRUE, NULL);
		g_return_val_if_fail (fname != NULL, NULL);
		xml = glade_xml_new (fname, NULL, NULL);
		g_free (fname);
	}

	return xml;
}
