/*
 * main.c: the main program / app list view
 *
 * Author:
 *    Michael Meeks
 *
 * Copyright 2002 Sun Microsystems, Inc.
 * Copyright 2005 Novell, Inc.
 */
#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include <cspi/spi.h>
#include <libgnomeui/gnome-ui-init.h>
#include <popt.h>
#include <gconf/gconf-client.h>

#include "graphics.h"
#include "eventlog.h"
#include "child-listener.h"
#include "accessible-listener.h"

/* Defaults */
int max_children = 64;
gboolean dontdisplaychildren = TRUE;
gboolean use_table_if = FALSE;
gboolean fork_gtk_demo = FALSE;
char *target_display = NULL;
static char *target_display_env = NULL;
static char *initial_display = NULL;

static const struct poptOption options[] = {
 	{ "maxchildren", 'm', POPT_ARG_INT, &max_children, 1,
	  "Specify the maximum number of children of an Accessible to be returned; or maximum ch ildren in a row or column if table is specified.\n", "INT"},
 	{ "dontdisplaychildren", 'd', POPT_ARG_NONE, &dontdisplaychildren, 1,
	  "Do not display children for object which has state MANAGES_DESCENDANTS\n", NULL},
 	{ "table", 't', POPT_ARG_NONE, &use_table_if, 1,
	  "Use AccessibleTable_getAccessibleAt to get children of Accessible which implements AccessibleTable\n", NULL},
 	{ "target-display", 'T', POPT_ARG_STRING, &target_display, 0, "Separate X display to poke at", NULL},
 	{ "demo", 'm', POPT_ARG_NONE, &fork_gtk_demo, 1, "Force a gtk-demo child process to be forked", NULL},
	{NULL, '\0', 0, NULL, 0, NULL, NULL}
};

static void
child_setup_env (gpointer dummy)
{
	putenv ("GTK_MODULES=gail:atk-bridge");
	putenv ("GNOME_ACCESSIBILITY=1");
	if (target_display)
		putenv (target_display_env);
}

static void
spawn_app (gchar **argv)
{
	GError *error = NULL;

	if (!argv || !argv[0])
		return;

	g_spawn_async (NULL, argv, NULL,
		       G_SPAWN_SEARCH_PATH,
		       child_setup_env, NULL,
		       NULL, &error);
	if (error) {
		g_warning ("Failed to spawn '%s' : '%s'",
			   argv[0], error->message);
		g_error_free (error);
	}
}

extern void poke (Accessible *accessible);
extern Accessible *get_accessible_at_index (GtkListStore *list_store, int i);

#define APP_COL_NAME   0
#define APP_COL_DESCR  1
#define APP_COL_HANDLE 2
#define APP_COL_LAST   3

#define GCONF_ACCESSIBILITY_KEY "/desktop/gnome/interface/accessibility"

typedef struct {
	GladeXML     *app_xml;

	GtkWidget    *window;
	
	GtkListStore *list_store;
	GtkTreeView  *list_view;

	GtkLabel     *toolkit_name;
	GtkLabel     *toolkit_version;
	GtkLabel     *app_id;

	Accessible   *selected;

	EventLog     *log;
} AppWindow;

static void
update_app_display (AppWindow  *app_window,
		    Accessible *accessible)
{
	char *txt;
	AccessibleApplication *application;

	if (!accessible ||
	    !(application = Accessible_getApplication (accessible))) {
		gtk_label_set_text (app_window->toolkit_name, "<invalid>");
		gtk_label_set_text (app_window->toolkit_version, "<invalid>");
		gtk_label_set_text (app_window->app_id, "<invalid>");
		return;
	}

	txt = AccessibleApplication_getToolkitName (application);
	gtk_label_set_text (app_window->toolkit_name, txt);
	SPI_freeString (txt);

	txt = AccessibleApplication_getVersion (application);
	gtk_label_set_text (app_window->toolkit_version, txt);
	SPI_freeString (txt);

	txt = g_strdup_printf ("%ld", AccessibleApplication_getID (application));
	gtk_label_set_text (app_window->app_id, txt);
	g_free (txt);

	AccessibleApplication_unref (application);
}

static void
app_list_selection_changed (GtkTreeSelection *selection,
			    AppWindow        *app_window)
{
	GtkTreeIter iter;
	Accessible *accessible;
	GtkTreeModel *model;
	
	if (!gtk_tree_selection_get_selected (selection, &model, &iter)) {
		return;
	}

	gtk_tree_model_get (model, &iter,
			    APP_COL_HANDLE, &accessible,
			    -1);
	update_app_display (app_window, accessible);

	app_window->selected = accessible;
}

static void
validate_up_down_linkage (Accessible *accessible)
{
	Accessible *child, *aa;
	Accessible *parent;

	aa = Accessible_queryInterface (accessible, "IDL:Accessibility/Accessible:1.0");

	g_assert (aa == accessible);

	Accessible_unref (aa);
	child = Accessible_getChildAtIndex (accessible, 0);
	if (!child)
		return;

	parent = Accessible_getParent (child);

	g_assert (parent == accessible);
	Accessible_unref (child);
	Accessible_unref (parent);
}

static void
app_list_row_activated (GtkTreeView       *tree_view,
			GtkTreePath       *path,
			GtkTreeViewColumn *column,
			AppWindow         *app_window)
{
	validate_up_down_linkage (app_window->selected);
	poke (app_window->selected);
}

Accessible *
get_accessible_at_index (GtkListStore *list_store, int i)
{
	GtkTreeIter iter;
	GtkTreeModel *model = GTK_TREE_MODEL (list_store);
	Accessible *accessible;
	gboolean ret;

	ret = gtk_tree_model_iter_nth_child (model, &iter, NULL, i);
	g_return_val_if_fail (ret, NULL);

	gtk_tree_model_get (model, &iter,
			    APP_COL_HANDLE, &accessible,
			    -1);
	return accessible;
}

static void
list_store_clear (GtkListStore *list_store)
{
	GtkTreeIter iter;
	GtkTreeModel *model = GTK_TREE_MODEL (list_store);
	Accessible *accessible;
	gboolean ret;

	ret = gtk_tree_model_get_iter_first (model, &iter);

	while (ret) {
		gtk_tree_model_get (model, &iter,
				    APP_COL_HANDLE, &accessible,
				    -1);
		Accessible_unref (accessible);
		ret = gtk_tree_model_iter_next (model, &iter);
	}
	gtk_list_store_clear (list_store);
}

static void
populate_app_list (AppWindow *app_window)
{
	int i, apps;
	Accessible *desktop;
	GtkTreeIter iter;

	desktop = SPI_getDesktop (0);

	apps = Accessible_getChildCount (desktop);

	list_store_clear (app_window->list_store);
	for (i = 0; i < apps; i++) {
		char *name, *descr;
		Accessible *child;

		child = Accessible_getChildAtIndex (desktop, i);
		if (!child)
			continue;

		name = Accessible_getName (child);
                if (!name || name[0] == '\0') {
			SPI_freeString (name);
			name = SPI_dupString ("<no name>");
		}

		descr = Accessible_getDescription (child);
		gtk_list_store_append (app_window->list_store, &iter);
		gtk_list_store_set (app_window->list_store,
				    &iter,
				    APP_COL_NAME,   name,
				    APP_COL_DESCR,  descr,
				    APP_COL_HANDLE, child,
				    -1);
		SPI_freeString (descr);
		SPI_freeString (name);
	}

	if (gtk_tree_model_get_iter_root (
		GTK_TREE_MODEL (app_window->list_store), &iter))
		gtk_tree_selection_select_iter (
			gtk_tree_view_get_selection (app_window->list_view), &iter);
	else
		app_window->selected = NULL;

	Accessible_unref (desktop);
}

static void
app_list_changed_cb (ChildListener *listener,
		     AppWindow     *window)
{
	populate_app_list (window);
}

static gboolean
window_delete (GtkObject *obj, GdkEventAny *event)
{
	gtk_main_quit ();
	return TRUE;
}

static void
create_app_list (AppWindow *app_window)
{
	app_window->list_store = 
		gtk_list_store_new (APP_COL_LAST,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_POINTER);

	app_window->list_view = GTK_TREE_VIEW (
		glade_xml_get_widget (
			app_window->app_xml, "application_list_view"));

	gtk_tree_view_set_model (app_window->list_view,
				 GTK_TREE_MODEL (app_window->list_store));

	gtk_tree_view_insert_column_with_attributes
		(app_window->list_view,
		 APP_COL_NAME, "Application",
		 gtk_cell_renderer_text_new (),
		 "text", 0,
		 NULL);
	gtk_tree_view_insert_column_with_attributes
		(app_window->list_view,
		 APP_COL_DESCR, "Description",
		 gtk_cell_renderer_text_new (),
		 "text", 0,
		 NULL);
	gtk_tree_view_columns_autosize (app_window->list_view);
	gtk_tree_selection_set_mode (
		gtk_tree_view_get_selection (
			app_window->list_view),
		GTK_SELECTION_BROWSE);

	g_signal_connect (
		gtk_tree_view_get_selection (
			app_window->list_view),
		"changed",
		G_CALLBACK (app_list_selection_changed),
		app_window);

	g_signal_connect (
		GTK_TREE_VIEW (app_window->list_view),
		"row_activated",
		G_CALLBACK (app_list_row_activated),
		app_window);
}

static void
application_poke_clicked (GtkButton *button,
			  AppWindow *app_window)
{
	if (!app_window->selected)
		gdk_beep ();
	else {
		validate_up_down_linkage (app_window->selected);
		poke (app_window->selected);
	}
}

static void
application_refresh_clicked (GtkButton *button,
			     AppWindow *app_window)
{
	populate_app_list (app_window);
}

static gboolean
event_log_delete_event (GtkWidget   *widget,
			GdkEventAny *event,
			AppWindow   *app_window)
{
	GtkToggleButton *toggle;
	toggle = GTK_TOGGLE_BUTTON (glade_xml_get_widget (app_window->app_xml,
							  "show_event_log") );
	gtk_toggle_button_set_active (toggle, FALSE);
	return TRUE;
}

static void
show_event_log_toggled (GtkToggleButton *button,
			AppWindow       *app_window)
{
	GtkToggleButton *toggle;
	toggle = GTK_TOGGLE_BUTTON (glade_xml_get_widget (app_window->app_xml,
							  "show_event_log") );

	if (gtk_toggle_button_get_active (toggle)) {
		if (!app_window->log) {
			app_window->log = event_log_create (GTK_WINDOW (app_window->window));
			g_signal_connect (event_log_get_window (app_window->log), "delete_event",
					  G_CALLBACK (event_log_delete_event), app_window);
		}
	} else {
		event_log_destroy (app_window->log);
		app_window->log = NULL;
	}
}

static void
file_execute_clicked (GtkButton *button,
		      AppWindow *app_window)
{
	GladeXML  *xml;
	GtkWidget *window;
	GtkEntry  *entry;

	xml = get_glade_xml();

	window = glade_xml_get_widget (xml, "execute");
	entry = GTK_ENTRY (glade_xml_get_widget (xml, "execute_name"));

	gtk_widget_show (window);
	if (gtk_dialog_run (GTK_DIALOG (window)) == GTK_RESPONSE_OK) {
		gchar **argv;
		argv = g_strsplit (gtk_entry_get_text (entry), " ", -1);
		spawn_app (argv);
		g_strfreev (argv);
	}
	gtk_widget_destroy (window);

	g_object_unref (xml);
}

static void
edit_preferences_clicked (GtkButton *button,
			  AppWindow *app_window)
{
	GladeXML *xml;
	GtkWidget *window;
	GtkCheckButton *pref_ignore_private;
	GtkCheckButton *pref_use_table_iface;
	GtkSpinButton  *pref_max_children;
	
	xml = get_glade_xml();

	window = glade_xml_get_widget (xml, "preferences");
	pref_ignore_private = GTK_CHECK_BUTTON
		(glade_xml_get_widget (xml, "pref_ignore_private"));
	pref_use_table_iface = GTK_CHECK_BUTTON
		(glade_xml_get_widget (xml, "pref_use_table_iface"));
	pref_max_children = GTK_SPIN_BUTTON
		(glade_xml_get_widget (xml, "pref_max_children"));

	g_object_set (pref_ignore_private,  "active", dontdisplaychildren, NULL );
	g_object_set (pref_use_table_iface, "active", use_table_if, NULL );
	g_object_set (pref_max_children,    "value",  (gdouble) max_children, NULL );

	gtk_widget_show (window);
	if (gtk_dialog_run (GTK_DIALOG (window)) == GTK_RESPONSE_OK) {
		max_children = gtk_spin_button_get_value (pref_max_children);
		g_object_get (pref_ignore_private,  "active", &dontdisplaychildren, NULL );
		g_object_get (pref_use_table_iface, "active", &use_table_if, NULL );
	}
	gtk_widget_destroy (window);

	g_object_unref (xml);
}

static void
file_quit_clicked (GtkButton *button,
		     AppWindow *app_window)
{
	gtk_main_quit();
}

static void
application_window (void)
{
	GClosure  *closure;
	GladeXML  *app_xml;
	AppWindow *app_window = g_new0 (AppWindow, 1);
	ChildListener *cl;

	app_xml = get_glade_xml ();
	app_window->app_xml = app_xml;

	if (!app_xml)
		g_error ("Can't find at-poke.glade2");

	app_window->window = glade_xml_get_widget (
		app_xml, "application_window");
	g_object_add_weak_pointer (G_OBJECT (app_window->window), (gpointer *)&(app_window->window));

	gtk_widget_show (app_window->window);

	create_app_list (app_window);

	app_window->toolkit_name = GTK_LABEL (
		glade_xml_get_widget (app_xml, "application_toolkit_name"));
	app_window->toolkit_version = GTK_LABEL (
		glade_xml_get_widget (app_xml, "application_toolkit_version"));
	app_window->app_id = GTK_LABEL (
		glade_xml_get_widget (app_xml, "application_id"));

	populate_app_list (app_window);

	/* Menu items */
	glade_xml_signal_connect_data (app_xml, "file_execute_clicked",
				       G_CALLBACK (file_execute_clicked),
				       app_window);
	glade_xml_signal_connect_data (app_xml, "edit_preferences_clicked",
				       G_CALLBACK (edit_preferences_clicked),
				       app_window);
	glade_xml_signal_connect_data (app_xml, "file_quit_clicked",
				       G_CALLBACK (file_quit_clicked),
				       app_window);

	/* Body widgets */
	glade_xml_signal_connect_data (app_xml, "application_poke_clicked",
				       G_CALLBACK (application_poke_clicked),
				       app_window);
	glade_xml_signal_connect_data (app_xml, "application_refresh_clicked",
				       G_CALLBACK (application_refresh_clicked),
				       app_window);
	glade_xml_signal_connect_data (app_xml, "show_event_log_toggled",
				       G_CALLBACK (show_event_log_toggled),
				       app_window);
	show_event_log_toggled (NULL, app_window);

	closure = g_cclosure_new (
		G_CALLBACK (app_list_changed_cb),
		app_window, NULL);
	g_object_watch_closure (
		G_OBJECT (app_window->window),
		closure);
	cl = child_listener_create (app_window->list_store);
	g_signal_connect_closure (
		cl,
		"app_list_changed",
		closure, FALSE);

	g_signal_connect (app_window->window,
			  "delete_event",
			  G_CALLBACK (window_delete),
			  NULL);

	gtk_main ();

	event_log_destroy (app_window->log);
	gtk_widget_destroy (app_window->window);

	list_store_clear (app_window->list_store);
	g_object_unref (app_xml);
	g_free (app_window);
	g_object_unref (cl);
	/*
	 * Deregister listeners which have been registered.
	 */
	accessible_listener_shutdown ();
}

static gboolean
no_accessible_apps (void)
{
	int num_apps;
	Accessible *desktop;

	desktop = SPI_getDesktop (0);

	num_apps = Accessible_getChildCount (desktop);
	
	Accessible_unref (desktop);

	return (num_apps == 0);
}

int
main (int argc, char **argv)
{
	int leaked;
	int init_error;
	poptContext ctxt;
	GnomeProgram *program;
	const char **spawn_args;
	GError *error = NULL;
	gboolean a11y_enabled;

	/* We don't want to show ourself */
	putenv ("GTK_MODULES=");
	putenv ("GNOME_ACCESSIBILITY=0");

	program = gnome_program_init ("at-poke", VERSION,
				      LIBGNOMEUI_MODULE,
				      argc, argv, 
				      GNOME_PARAM_POPT_TABLE, options,
				      GNOME_PARAM_APP_DATADIR, DATADIR,
				      NULL);
	if (max_children < 0)
		max_children = 0;

	initial_display = g_strdup_printf ("DISPLAY=%s",g_getenv("DISPLAY"));
	if (target_display) {
		target_display_env = g_strdup_printf ("DISPLAY=%s",target_display);
		putenv (target_display_env);
	}
	g_warning ("initial '%s' target '%s'", initial_display, target_display ? target_display : "NULL");

	init_error = SPI_init ();
	if (init_error) {
	        g_warning ("Error initialising SPI");
	        return init_error;
	}

	if (target_display)
		putenv (initial_display);

	g_object_get (program, GNOME_PARAM_POPT_CONTEXT, &ctxt, NULL);
	spawn_args = poptGetArgs (ctxt);

	a11y_enabled = gconf_client_get_bool (gconf_client_get_default (),
					      GCONF_ACCESSIBILITY_KEY, &error);

	if (((!spawn_args || !spawn_args[0]) && no_accessible_apps()) ||
	    fork_gtk_demo)
	{
		GtkWidget *dialog;
		char *demo_args[] = { "gtk-demo", NULL };
		dialog = gtk_message_dialog_new (NULL,
						 GTK_DIALOG_MODAL,
						 GTK_MESSAGE_INFO,
						 GTK_BUTTONS_OK,
						 "No accessible apps: at-poke will launch "
						 "gtk-demo with accessibility enabled");
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		spawn_app (demo_args);
	} else
		spawn_app ((char **)spawn_args);

	application_window ();

	if ((leaked = SPI_exit ()))
		g_error ("Leaked %d SPI handles", leaked);

	g_assert (!SPI_exit ());

	if (g_getenv ("_MEMPROF_SOCKET")) {
		fprintf (stderr, "Waiting for memprof\n");
		gtk_main ();
	}

	putenv ("AT_BRIDGE_SHUTDOWN=1");

	return 0;
}
