/*
 * main.c: A simple at-poking program
 *
 * Author:
 *    Michael Meeks
 *
 * Copyright 2002 Sun Microsystems, Inc.
 */
#ifndef ACCESSIBLE_TREE_MODEL_H
#define ACCESSIBLE_TREE_MODEL_H

#include <cspi/spi.h>
#include <glib-object.h>
#include <gtk/gtktreemodel.h>

#define ACCESSIBLE_TYPE_TREE_MODEL	    (accessible_tree_model_get_type ())
#define ACCESSIBLE_TREE_MODEL(obj)	    (GTK_CHECK_CAST ((obj), ACCESSIBLE_TYPE_TREE_MODEL, AccessibleTreeModel))
#define ACCESSIBLE_TREE_MODEL_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), ACCESSIBLE_TYPE_TREE_MODEL, AccessibleTreeModelClass))
#define ACCESSIBLE_IS_TREE_MODEL(obj)	    (GTK_CHECK_TYPE ((obj), ACCESSIBLE_TYPE_TREE_MODEL))
#define ACCESSIBLE_IS_TREE_MODEL_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), ACCESSIBLE_TYPE_TREE_MODEL))

enum {
	ACCESSIBLE_TREE_MODEL_NAME_COL,
	ACCESSIBLE_TREE_MODEL_ROLE_COL,
	ACCESSIBLE_TREE_MODEL_DESCR_COL,
	ACCESSIBLE_TREE_MODEL_ICON_COL,
	ACCESSIBLE_TREE_MODEL_NUM_COLS
};

typedef struct {
	GObject parent;

	int                      stamp;
	AccessibleEventListener *event_listener;

	GNode                   *root;
	GHashTable              *accessible_to_node;
} AccessibleTreeModel;

typedef struct {
	GObjectClass parent_class;
} AccessibleTreeModelClass;

GType         accessible_tree_model_get_type (void);
GtkTreeModel *accessible_tree_model_new      (Accessible  *opt_root);
Accessible   *accessible_tree_get_from_iter  (GtkTreeIter *iter);
gboolean      accessible_is_base_accessible  (Accessible  *accessible);
char         *accessible_tree_model_path     (AccessibleTreeModel *model,
					      Accessible          *accessible);
gboolean      accessible_tree_owns_node      (AccessibleTreeModel *model,
					      Accessible          *accessible);
char         *accessible_get_path  (Accessible *accessible);
Accessible   *accessible_from_path (const char *path);

#endif /* ACCESSIBLE_TREE_MODEL_H */
