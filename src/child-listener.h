/*
 * child-listener.h: tries to de-mangle the
 *                   children-changed events
 *
 * Author:
 *    Michael Meeks
 *
 * Copyright 2002 Sun Microsystems, Inc.
 */
#ifndef CHILD_LISTENER_H
#define CHILD_LISTENER_H

#include <cspi/spi.h>
#include <glib-object.h>
#include <gtk/gtkliststore.h>

typedef struct {
	GObject parent;

	GSList *roots;
	Accessible *desktop;
	AccessibleEventListener *el;
	GtkListStore *list_store;
} ChildListener;

typedef struct {
	GObjectClass parent_class;

	/* Signals */
	void (*app_list_changed) (ChildListener *listener);
	void (*root_died)        (ChildListener *listener,
				  Accessible    *root);
	void (*children_changed) (ChildListener *listener,
				  Accessible    *accessible);
} ChildListenerClass;

ChildListener *child_listener_create      (GtkListStore  *list_store);
ChildListener *child_listener_get         (void);
void           child_listener_add_root    (ChildListener *listener,
					   Accessible    *root);
void           child_listener_remove_root (ChildListener *listener,
					   Accessible    *root);

#endif /* CHILDREN_LISTENER_H */
