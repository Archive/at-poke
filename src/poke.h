#ifndef POKE_H
#define POKE_H

extern int max_children;
extern gboolean use_table_if;
extern char *target_display;
extern void poke (Accessible *accessible);
extern char *accessible_get_iface_string (Accessible *accessible);

#endif
