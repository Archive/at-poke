#ifndef EVENTLOG_H
#define EVENTLOG_H

#include <glib/gtypes.h>

typedef struct _EventLog EventLog;

EventLog  *event_log_create     (GtkWindow *parent);
GtkWindow *event_log_get_window (EventLog *log);
void       event_log_destroy    (EventLog  *log);
void       event_log_enable     (EventLog  *log, gboolean visible);

#endif
