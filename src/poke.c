/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * poke.c: the poker / accessible tree view
 *
 * Author:
 *    Michael Meeks
 *
 * Copyright 2002 Sun Microsystems, Inc.
 */

/*
 * TODO:
 *    + add 'getAccessibleAtPoint' on
 *      toplevel whatnot - if possible
 *    + Text selection view
 *    + AccessibleHyperlink
 *    + editable text
 *    + sync text caret spinbutton with text entry field, and changes to
 *      length of text string.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <cspi/spi.h>
#include <glade/glade-xml.h>
#include <libgnomeui/gnome-ui-init.h>

#include "poke.h"
#include "graphics.h"
#include "child-listener.h"
#include "accessible-listener.h"
#include "accessible-tree-model.h"

#define BLINK_INTERVAL_MS 200

static GList *poker_list = NULL;

typedef struct {
	GladeXML     *xml;

	Accessible   *root_node;

	GtkWidget    *window;

	GtkWidget    *accessible_role;
	GtkWidget    *accessible_role_image;
	GtkWidget    *accessible_relations_frame;
	GtkWidget    *accessible_states_frame;

	GtkTreeModel *tree_model;
	GtkTreeView  *tree_view;

	GtkListStore *state_set_store;
	GtkListStore *relation_store;
	GtkListStore *action_store;
	GtkWidget    *action_view;
	GtkWidget    *action_take;

	GtkWidget    *text;
	GtkWidget    *text_caret;
	GtkLabel     *text_frame_label;
	GtkWidget    *attribute_label;
	gulong        text_changed_id;
	gulong        caret_changed_id;

	GtkWidget    *value_minimum;
	GtkWidget    *value_current;
	GtkWidget    *value_maximum;
	gulong        value_changed_id;

	GtkWidget    *image_descr;
	GtkWidget    *image_size;

	GtkListStore *selection_store;
	GtkTreeView  *selection_view;
	GtkWidget    *selection_all;
	GtkWidget    *selection_clear;
	gulong        selection_changed_id;

	GtkListStore *table_columns_store;
	GtkListStore *table_rows_store;

	gulong        update_id;
	gulong        text_update_id;

	GtkListStore *hypertext_store;
	GtkTreeView  *hypertext_view;

	GtkWidget    *document_locale;
	GtkWidget    *document_attributes;

	GtkContainer *streamable_container;
	GtkComboBox  *streamable_combo;
	GtkTextView  *streamable_textview;

	Accessible   *selected;

	AccessibleCoordType ctype;
} Poker;

typedef struct {
	gint count;
	gint x;
	gint y;
	gint w;
	gint h;
} BlinkClosure;

enum {
	POKER_SELECTION_ICON,
	POKER_SELECTION_NAME,
	POKER_SELECTION_DESCR,
	POKER_SELECTION_IDX
};

enum {
	RELATION_NAME,
	RELATION_LABEL,
	RELATION_PATH
};

static void
poker_select_accessible (Poker *poker, const char *path)
{
	GtkTreePath *main_path;
	GtkTreeSelection *main_selection;

	main_path = gtk_tree_path_new_from_string (path);
	main_selection = gtk_tree_view_get_selection (poker->tree_view);
	gtk_tree_selection_unselect_all (main_selection);
	gtk_tree_selection_select_path (main_selection, main_path);

	gtk_tree_path_free (main_path);
}

static void
relation_row_activated_cb (GtkTreeView       *tree_view,
			   GtkTreePath       *path,
			   GtkTreeViewColumn *column,
			   Poker             *poker)
{
	char *path_str;
	GtkTreeIter iter;
	GtkTreeModel *model;

	model = GTK_TREE_MODEL (poker->relation_store);

	gtk_tree_model_get_iter (model, &iter, path);

	/* if the tree has changed, this will break - sigh */
	g_warning ("This may select totally the wrong accessible");

	gtk_tree_model_get (model, &iter, RELATION_PATH, &path_str, -1);

	poker_select_accessible (poker, path_str);

	g_free (path_str);
}

static void
update_relation_set (Poker *poker, AccessibleRelation **relations)
{
	int i;
	static const struct {
		AccessibleRelationType type;
		const char            *name;
	} relation_names [] = {
		{ SPI_RELATION_NULL,           "NULL" },
		{ SPI_RELATION_LABEL_FOR,      "LABEL_FOR" },
		{ SPI_RELATION_LABELED_BY,     "LABELLED_BY" },
		{ SPI_RELATION_CONTROLLER_FOR, "CONTROLLER_FOR" },
		{ SPI_RELATION_CONTROLLED_BY,  "CONTROLLED_BY" },
		{ SPI_RELATION_MEMBER_OF,      "MEMBER_OF" },
		{ SPI_RELATION_NODE_CHILD_OF,  "NODE_CHILD_OF" },
		{ SPI_RELATION_EXTENDED,       "EXTENDED" },
		{ SPI_RELATION_FLOWS_TO,       "FLOWS_TO" },
		{ SPI_RELATION_FLOWS_FROM,     "FLOWS_FROM" },
		{ SPI_RELATION_SUBWINDOW_OF,   "SUBWINDOW_OF" },
		{ SPI_RELATION_EMBEDS,         "EMBEDS" },
		{ SPI_RELATION_EMBEDDED_BY,    "EMBEDDED_BY" },
		{ SPI_RELATION_POPUP_FOR,      "POPUP_FOR" },
		{ 0, NULL }
	};

	if (relations == NULL || *relations == NULL) {
		gtk_widget_hide (poker->accessible_relations_frame);
		return;
	}

	gtk_list_store_clear (poker->relation_store);

	for (i = 0; relations [i]; i++) {
		int j, num_targets;
		GtkTreeIter iter;
		const char *type_name = "None";
		AccessibleRelation *relation = relations [i];
		AccessibleRelationType type;

		type = AccessibleRelation_getRelationType (relations [i]);

		for (j = 0; relation_names [j].name; j++) {
			if (relation_names [j].type == type)
				type_name = relation_names [j].name;
		}

		num_targets = AccessibleRelation_getNTargets (relations [i]);
		if (num_targets <= 0)
			g_warning ("Very odd an accessible relation "
				   "with %d targets", num_targets);

		for (j = 0; j < num_targets; j++) {
			char *label;
			char *path;
			Accessible *target;

			target = AccessibleRelation_getTarget (relations [i], j);
			
			if (!target)
				g_warning ("Very odd relation target %d is NULL", j);

			label = Accessible_getName (target);
			path = accessible_tree_model_path (
				ACCESSIBLE_TREE_MODEL (poker->tree_model),
				target);

			gtk_list_store_append (poker->relation_store, &iter);
			gtk_list_store_set (poker->relation_store, &iter,
					    RELATION_NAME, type_name,
					    RELATION_LABEL, label,
					    RELATION_PATH, path,
					    -1);
			g_free (path);
			SPI_freeString (label);
		}

		AccessibleRelation_unref (relation);
	}

	gtk_widget_show (poker->accessible_relations_frame);
}

static void
update_if_accessible (Poker *poker, Accessible *accessible)
{
	int i;
	char *txt, *txt2;
	gboolean has_state;
	AccessibleRelation **relations;
	AccessibleStateSet *state_set;
	static const struct {
		AccessibleState state;
		const char     *name;
	} state_names [] = {
		{ SPI_STATE_INVALID, "INVALID" },
		{ SPI_STATE_ACTIVE, "ACTIVE" },
		{ SPI_STATE_ARMED, "ARMED" },
		{ SPI_STATE_BUSY, "BUSY" },
		{ SPI_STATE_CHECKED, "CHECKED" },
		{ SPI_STATE_COLLAPSED, "COLLAPSED" },
		{ SPI_STATE_DEFUNCT, "DEFUNCT" },
		{ SPI_STATE_EDITABLE, "EDITABLE" },
		{ SPI_STATE_ENABLED, "ENABLED" },
		{ SPI_STATE_EXPANDABLE, "EXPANDABLE" },
		{ SPI_STATE_EXPANDED, "EXPANDED" },
		{ SPI_STATE_FOCUSABLE, "FOCUSABLE" },
		{ SPI_STATE_FOCUSED, "FOCUSED" },
		{ SPI_STATE_HORIZONTAL, "HORIZONTAL" },
		{ SPI_STATE_ICONIFIED, "ICONIFIED" },
		{ SPI_STATE_MANAGES_DESCENDANTS, "MANAGES_DESCENDANTS" },
		{ SPI_STATE_MODAL, "MODAL" },
		{ SPI_STATE_MULTI_LINE, "MULTI_LINE" },
		{ SPI_STATE_MULTISELECTABLE, "MULTISELECTABLE" },
		{ SPI_STATE_OPAQUE, "OPAQUE" },
		{ SPI_STATE_PRESSED, "PRESSED" },
		{ SPI_STATE_RESIZABLE, "RESIZABLE" },
		{ SPI_STATE_SELECTABLE, "SELECTABLE" },
		{ SPI_STATE_SELECTED, "SELECTED" },
		{ SPI_STATE_SENSITIVE, "SENSITIVE" },
		{ SPI_STATE_SHOWING, "SHOWING" },
		{ SPI_STATE_SINGLE_LINE, "SINGLE_LINE" },
		{ SPI_STATE_STALE, "STALE" },
		{ SPI_STATE_TRANSIENT, "TRANSIENT" },
		{ SPI_STATE_VERTICAL, "VERTICAL" },
		{ SPI_STATE_VISIBLE, "VISIBLE" },
		{ 0, NULL }
	};

	txt = Accessible_getRoleName (accessible);
	txt2 = g_strdup_printf ("<big><b>%s</b></big>",
				txt && txt [0] != '\0' ? txt : "<no role>");
	gtk_label_set_markup (GTK_LABEL (poker->accessible_role), txt2);
	g_free (txt2);
	SPI_freeString (txt);
	
	gtk_image_set_from_pixbuf (
		GTK_IMAGE (poker->accessible_role_image),
		get_pixbuf_for_role (Accessible_getRole (accessible)));

	relations = Accessible_getRelationSet (accessible);
	update_relation_set (poker, relations);
	free (relations); /* FIXME: need a method for this */

	has_state = FALSE;
	gtk_list_store_clear (poker->state_set_store);
	state_set = Accessible_getStateSet (accessible);
	for (i = 0; state_names [i].name; i++) {
		if (AccessibleStateSet_contains (state_set,
						 state_names [i].state)) {
			GtkTreeIter iter;
			gtk_list_store_append (poker->state_set_store, &iter);
			gtk_list_store_set (poker->state_set_store, &iter,
					    0, state_names [i].name, -1);
			has_state = TRUE;
		}
	}
	AccessibleStateSet_unref (state_set);

	if (has_state)
		gtk_widget_show (poker->accessible_states_frame);
	else
		gtk_widget_hide (poker->accessible_states_frame);
}

static void
component_grab_focus_clicked (GtkButton *button, Poker *poker)
{
	AccessibleComponent *component;

	g_return_if_fail (poker->selected != NULL);

	component = Accessible_getComponent (poker->selected);
	g_return_if_fail (component != NULL);

	g_warning ("Grab focus !");
	AccessibleComponent_grabFocus (component);

	AccessibleComponent_unref (component);
}

static gboolean
blink_draw_rectangle (gpointer user_data)
{
	static GtkWidget *blink_widget = NULL;
	static GdkDisplay *display = NULL;
	BlinkClosure *blink_data = user_data;
	GdkScreen *screen;
	GdkWindow *root_window;

	if (!display) {
		if (target_display) {
			g_warning ("Target Display is '%s'\n", target_display);
			display = gdk_display_open (target_display);
		} else
			display = gdk_display_get_default ();
	}
	g_return_val_if_fail (display != NULL, TRUE);

	screen = gdk_display_get_default_screen (display);
	root_window = gdk_screen_get_root_window (screen);
 
	GdkGC *gc = gdk_gc_new (root_window);

	/* grab mouse for the invisible widget when we are blinking */
	if (!blink_widget) {
		blink_widget = gtk_invisible_new_for_screen (screen);
		gtk_widget_show (blink_widget);
		gtk_grab_add (blink_widget);
	}

	/* set subwindow mode, function and line attributes for our GC */
	gdk_gc_set_subwindow (gc, GDK_INCLUDE_INFERIORS);
	gdk_gc_set_function (gc, GDK_INVERT);
	gdk_gc_set_line_attributes (gc, 3, GDK_LINE_SOLID,
				    GDK_CAP_BUTT, GDK_JOIN_MITER);
	gdk_draw_rectangle (root_window, gc, FALSE,
			    blink_data->x, blink_data->y,
			    blink_data->w, blink_data->h);
	g_object_unref (gc);

	--(blink_data->count);

	/* return FALSE will destroy the timer. */
	if (blink_data->count <= 0) {
		gtk_grab_remove (blink_widget);
		gtk_widget_destroy (blink_widget);
		blink_widget = NULL;
		g_free (blink_data);
		return FALSE;
	}
	return TRUE;
}

static void
component_blink_extents (long int x, long int y, long int w, long int h)
{
	BlinkClosure *blink_data = g_new0 (BlinkClosure, 1);

	blink_data->count = 4;  /* should be an even number */
	blink_data->x = x;
	blink_data->y = y;
	blink_data->w = w;
	blink_data->h = h;

	g_timeout_add (BLINK_INTERVAL_MS, blink_draw_rectangle, blink_data);
}

static void
update_if_component (Poker *poker, AccessibleComponent *component)
{
	int i;
	short zorder;
	long int x, y, w, h;
	char *txt;
	GtkWidget *widget;
	AccessibleComponentLayer layer;
	static const struct {
		AccessibleComponentLayer layer;
		const char              *name;
	} layer_names [] = {
		{ SPI_LAYER_INVALID, "INVALID" },
		{ SPI_LAYER_BACKGROUND, "BACKGROUND" },
		{ SPI_LAYER_CANVAS, "CANVAS" },
		{ SPI_LAYER_WIDGET, "WIDGET" },
		{ SPI_LAYER_MDI, "MDI" },
		{ SPI_LAYER_POPUP, "POPUP" },
		{ SPI_LAYER_OVERLAY, "OVERLAY" },
		{ 0, NULL }
	};

	/* make our blinks */
	AccessibleComponent_getExtents (component, &x, &y, &w, &h,
					SPI_COORD_TYPE_SCREEN);
	component_blink_extents (x, y, w, h);

	if (poker->ctype != SPI_COORD_TYPE_SCREEN)
		AccessibleComponent_getExtents (component, &x, &y, &w, &h,
						poker->ctype);

	widget = glade_xml_get_widget (poker->xml, "component_if_position");
	txt = g_strdup_printf ("%ld, %ld", x, y);
	gtk_label_set_text (GTK_LABEL (widget), txt);
	g_free (txt);

	widget = glade_xml_get_widget (poker->xml, "component_if_size");
	txt = g_strdup_printf ("%ld, %ld", w, h);
	gtk_label_set_text (GTK_LABEL (widget), txt);
	g_free (txt);

	txt = "<invalid layer>";
	layer = AccessibleComponent_getLayer (component);
	for (i = 0; layer_names [i].name; i++) {
		if (layer_names [i].layer == layer)
			txt = (char *) layer_names [i].name;
	}
	widget = glade_xml_get_widget (poker->xml, "component_if_layer");
	gtk_label_set_text (GTK_LABEL (widget), txt);

	zorder = AccessibleComponent_getMDIZOrder (component);
	widget = glade_xml_get_widget (poker->xml, "component_if_zorder");
	txt = g_strdup_printf ("%d", (int) zorder);
	gtk_label_set_text (GTK_LABEL (widget), txt);
	g_free (txt);
}

static void
action_take_clicked (GtkButton *button, Poker *poker)
{
	AccessibleAction *action;
	GtkTreeSelection *selection;
	GtkTreeIter iter;

	g_return_if_fail (poker->selected != NULL);

	action = Accessible_getAction (poker->selected);
	g_return_if_fail (action != NULL);

	selection = gtk_tree_view_get_selection (
		GTK_TREE_VIEW (poker->action_view));
	if (selection
	    && gtk_tree_selection_get_selected (selection, NULL, &iter)) {
		GtkTreePath *path;
		path = gtk_tree_model_get_path (
			GTK_TREE_MODEL (poker->action_store), &iter);
		
		AccessibleAction_doAction (action, 
					   gtk_tree_path_get_indices (path)[0]);
		
		gtk_tree_path_free (path);
	}

	AccessibleAction_unref (action);
}

static void
action_row_activated_cb (GtkTreeView       *tree_view,
			 GtkTreePath       *path,
			 GtkTreeViewColumn *column,
			 Poker             *poker)
{
	action_take_clicked (NULL, poker);
}

static void
link_activated_cb (GtkTreeView       *tree_view,
		   GtkTreePath       *path,
		   GtkTreeViewColumn *column,
		   Poker             *poker)
{
	gint index;
	AccessibleHypertext *hypertext;
	AccessibleHyperlink *link;
	AccessibleAction *action;

	index = gtk_tree_path_get_indices (path) [0];

	g_return_if_fail (poker->selected);

	hypertext = Accessible_getHypertext (poker->selected);
	g_return_if_fail (hypertext);
	link = AccessibleHypertext_getLink (hypertext, index);
	g_return_if_fail (link);
	action = Accessible_getAction (link);
	AccessibleAction_doAction (action, 0);

	Accessible_unref (action);
	Accessible_unref (link);
	Accessible_unref (hypertext);
}

static void
update_if_action (Poker *poker, AccessibleAction *action)
{
	long i, num_actions;
	GtkTreeIter iter;

	num_actions = AccessibleAction_getNActions (action);

	if( num_actions > 256 )
	{
		GtkWidget *dialog;
		char *msg = g_strdup_printf ("This accessible has too many actions %ld, cropping to 16",
					     num_actions);
		dialog = gtk_message_dialog_new (NULL,
						 GTK_DIALOG_MODAL,
						 GTK_MESSAGE_WARNING,
						 GTK_BUTTONS_OK,
						 msg );
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		num_actions = 16;
	}

	gtk_list_store_clear (poker->action_store);
	for (i = 0; num_actions > 0 && i < num_actions; i++) {
		char *name, *descr, *keybinding;
		
		name = AccessibleAction_getName (action, i);
		descr = AccessibleAction_getDescription (action, i);
		keybinding = AccessibleAction_getKeyBinding (action, i);

		gtk_list_store_append (poker->action_store, &iter);
		gtk_list_store_set (poker->action_store, &iter,
				    0, name, 1, descr, 2, keybinding, -1);

		SPI_freeString (keybinding);
		SPI_freeString (descr);
		SPI_freeString (name);
	}

	if (gtk_tree_model_get_iter_root (GTK_TREE_MODEL (poker->action_store),
					  &iter)) {
		gtk_tree_selection_select_iter (
			gtk_tree_view_get_selection (
				GTK_TREE_VIEW (poker->action_view)), &iter);
	}

	gtk_widget_set_sensitive (poker->action_take, num_actions > 0);
}

static void
update_text_caret (Poker *poker, AccessibleText *text)
{
	gint caret_pos;
	long int startOffset = 0, endOffset = 0;
	char *attributes;
	gchar *attribute_label_string;

	gtk_spin_button_set_range (
		GTK_SPIN_BUTTON (poker->text_caret),
		0, AccessibleText_getCharacterCount (text));

	caret_pos = AccessibleText_getCaretOffset (text);
	gtk_spin_button_set_value (
		GTK_SPIN_BUTTON (poker->text_caret),
		caret_pos);

	attributes = AccessibleText_getAttributes (text,
						   caret_pos,
						   &startOffset,
						   &endOffset);

	attribute_label_string = g_strconcat ("<i>",
		(attributes && strlen (g_strstrip (attributes)) ?
		 attributes : "(none)"), "</i>", NULL);

	gtk_label_set_markup (GTK_LABEL (poker->attribute_label),
			      attribute_label_string);
	if (attributes != NULL)
		SPI_freeString (attributes);
	
	g_free (attribute_label_string);
}

static void
update_if_text (Poker *poker, AccessibleText *text)
{
	char *txt;

	g_signal_handler_block (poker->text, poker->text_changed_id);
	g_signal_handler_block (poker->text_caret, poker->caret_changed_id);

	txt = AccessibleText_getText (text, 0, -1);
	gtk_entry_set_text (GTK_ENTRY (poker->text), txt);
	SPI_freeString (txt);

	gtk_entry_set_editable (GTK_ENTRY (poker->text), FALSE);
	gtk_label_set_text (poker->text_frame_label, "Static Text");

	g_signal_handler_unblock (poker->text, poker->text_changed_id);

	update_text_caret (poker, text);

	g_signal_handler_unblock (poker->text_caret, poker->caret_changed_id);
}

static void
editable_text_changed_cb (GtkEditable *editable_widget,
			  Poker       *poker)
{
	const char *txt;
	AccessibleText *text;
	AccessibleEditableText *editable;

	g_return_if_fail (poker->selected != NULL);

	editable = Accessible_getEditableText (poker->selected);
	g_return_if_fail (editable != NULL);

	txt = gtk_entry_get_text (GTK_ENTRY (editable_widget));
	AccessibleEditableText_setTextContents (editable, txt);

	AccessibleEditableText_unref (editable);

	if ((text = Accessible_getText (poker->selected) )) {
		update_text_caret (poker, text);
		AccessibleText_unref (text);
	}
}

static void
text_caret_changed_cb (GtkSpinButton *spin_button,
			Poker         *poker)
{
	AccessibleText *text;

	g_return_if_fail (poker->selected != NULL);

	text = Accessible_getText (poker->selected);
	g_return_if_fail (text != NULL);

	AccessibleText_setCaretOffset (
		text,
		gtk_spin_button_get_value (spin_button));

	AccessibleText_unref (text);
}


static void
update_if_editable (Poker *poker, AccessibleEditableText *editable)
{
	gtk_entry_set_editable (GTK_ENTRY (poker->text), TRUE);
	gtk_label_set_text (poker->text_frame_label, "Editable Text");
}

static void
get_selection_n_editable (Poker                   *poker,
			  AccessibleEditableText **etext,
			  long int                *start,
			  long int                *end)
{
	AccessibleText *text;

	*etext = NULL;

	g_return_if_fail (poker->selected != NULL);
	
	text = Accessible_getText (poker->selected);

	AccessibleText_getSelection (text, 0, start, end);

	*etext = Accessible_getEditableText (poker->selected);

	AccessibleText_unref (text);
}

static void
update_text (Poker *poker)
{
	AccessibleText *text = Accessible_getText (poker->selected);

	g_return_if_fail (text != NULL);

	update_if_text (poker, text);
	update_if_editable (poker, NULL);

	AccessibleText_unref (text);
}

static void
editable_cut_clicked_cb (GtkButton *button, Poker *poker)
{
	long int start, end;
	AccessibleEditableText *etext;

	get_selection_n_editable (poker, &etext, &start, &end);
	g_return_if_fail (etext != NULL);

	AccessibleEditableText_cutText (etext, start, end);

	update_text (poker);

	AccessibleEditableText_unref (etext);
}

static void
editable_copy_clicked_cb (GtkButton *button, Poker *poker)
{
	long int start, end;
	AccessibleEditableText *etext;

	get_selection_n_editable (poker, &etext, &start, &end);
	g_return_if_fail (etext != NULL);

	AccessibleEditableText_copyText (etext, start, end);

	update_text (poker);

	AccessibleEditableText_unref (etext);
}

static void
editable_delete_clicked_cb (GtkButton *button, Poker *poker)
{
	long int start, end;
	AccessibleEditableText *etext;

	get_selection_n_editable (poker, &etext, &start, &end);
	g_return_if_fail (etext != NULL);

	AccessibleEditableText_deleteText (etext, start, end);

	update_text (poker);

	AccessibleEditableText_unref (etext);
}

static void
editable_paste_clicked_cb (GtkButton *button, Poker *poker)
{
	long int start, end;
	AccessibleEditableText *etext;

	get_selection_n_editable (poker, &etext, &start, &end);
	g_return_if_fail (etext != NULL);

	AccessibleEditableText_pasteText (etext, start);

	update_text (poker);

	AccessibleEditableText_unref (etext);
}

static void
value_text_changed_cb (GtkEditable *txt_widget,
		       Poker       *poker)
{
	const char *txt;
	AccessibleValue *value;

	g_return_if_fail (poker->selected != NULL);

	value = Accessible_getValue (poker->selected);
	g_return_if_fail (value != NULL);

	txt = gtk_entry_get_text (GTK_ENTRY (txt_widget));

	AccessibleValue_setCurrentValue (value, atof (txt));

	AccessibleValue_unref (value);
}

static void
update_if_value (Poker *poker, AccessibleValue *value)
{
	double v;
	char  *txt;

	v = AccessibleValue_getMinimumValue (value);
	txt = g_strdup_printf ("%g", v);
	gtk_label_set_text (GTK_LABEL (poker->value_minimum), txt);
	g_free (txt);

	v = AccessibleValue_getMaximumValue (value);
	txt = g_strdup_printf ("%g", v);
	gtk_label_set_text (GTK_LABEL (poker->value_maximum), txt);
	g_free (txt);

	g_signal_handler_block (poker->value_current,
				poker->value_changed_id);

	v = AccessibleValue_getCurrentValue (value);
	txt = g_strdup_printf ("%g", v);
	gtk_entry_set_text (GTK_ENTRY (poker->value_current), txt);
	g_free (txt);
	
	g_signal_handler_unblock (poker->value_current,
				  poker->value_changed_id);
}

static void
update_if_table (Poker *poker, AccessibleTable *table)
{
	long int rows, cols;
	char *txt;
	GtkWidget *widget;
	int i;

	rows = AccessibleTable_getNRows (table);
	cols = AccessibleTable_getNColumns (table);
	if (max_children) {
		if (rows > max_children)
			rows = max_children;
		if (cols > max_children)
			cols = max_children;
	}

	widget = glade_xml_get_widget (poker->xml, "table_if_rows");
	txt = g_strdup_printf ("%ld", rows);
	gtk_label_set_text (GTK_LABEL (widget), txt);
	g_free (txt);

	widget = glade_xml_get_widget (poker->xml, "table_if_columns");
	txt = g_strdup_printf ("%ld", cols);
	gtk_label_set_text (GTK_LABEL (widget), txt);
	g_free (txt);

	gtk_list_store_clear (poker->table_rows_store);
	for (i = 0; i < rows; i++) {
		GtkTreeIter iter;
		char *description;
		int extent;

		description = AccessibleTable_getRowDescription (table, i);
		extent =  AccessibleTable_getRowExtentAt (table, i, 0);
		gtk_list_store_append (poker->table_rows_store, &iter);
		gtk_list_store_set (poker->table_rows_store, &iter,
				    0, description,
				    1, extent,
				    -1);
	}

	gtk_list_store_clear (poker->table_columns_store);
	for (i = 0; i < cols; i++) {
		GtkTreeIter iter;
		char *description =
			AccessibleTable_getColumnDescription (table, i);
		int extent =
			AccessibleTable_getColumnExtentAt (table, 0, i);
		gtk_list_store_append (poker->table_columns_store, &iter);
		gtk_list_store_set (poker->table_columns_store, &iter,
				    0, description,
				    1, extent,
				    -1);
	}
}

static void
update_if_image (Poker *poker, AccessibleImage *image)
{
	char *txt;
	long int w, h;

	txt = AccessibleImage_getImageDescription (image);

	gtk_label_set_text (GTK_LABEL (poker->image_descr),
			    txt && txt [0] != '\0' ? txt : "<no description>");
	SPI_freeString (txt);

	AccessibleImage_getImageSize (image, &w, &h);
	txt = g_strdup_printf ("%ld, %ld", w, h);
	gtk_label_set_text (GTK_LABEL (poker->image_size), txt);
	g_free (txt);
}

static void
update_if_document (Poker *poker, AccessibleDocument *document)
{
	char *docType, *locale;
	AccessibleAttributeSet *attributeSet;
	GString *attribute_str = g_string_new ("");
	int i;
 
 	locale = AccessibleDocument_getLocale (document);
 	gtk_label_set_text (GTK_LABEL (poker->document_locale), locale ? locale : "<no value>");
 
 	attributeSet = AccessibleDocument_getAttributes (document);
 	for ( i = 0; i < attributeSet->len; ++ i)
 	  {
	    
 	    g_string_append_printf (attribute_str,
 			       "%s%s%s", (i == 0) ? "" : " ",
 			       attributeSet->attributes [i],
 			       (i != attributeSet->len - 1) ? ";" : "");
 	  }
 	g_free (attributeSet);
 	gtk_label_set_text (GTK_LABEL (poker->document_attributes), 
			    g_string_free (attribute_str, FALSE));
 } 

static void
update_if_streamable (Poker *poker, AccessibleStreamableContent *streamable)
{
	char **mimetypes;
	gint i = 0;
	GtkComboBox *combo = GTK_COMBO_BOX (gtk_combo_box_new_text ());

	if (poker->streamable_combo) {
		gtk_container_remove (poker->streamable_container, 
				      GTK_WIDGET (poker->streamable_combo));
	}

	mimetypes = AccessibleStreamableContent_getContentTypes (streamable);
	while (mimetypes[i] != NULL) {
		gtk_combo_box_append_text (combo, g_strdup (mimetypes[i]));
		gtk_combo_box_set_active (combo, i);
		++i;
	}
	AccessibleStreamableContent_freeContentTypesList (streamable, mimetypes);
	poker->streamable_combo = combo;
	gtk_container_add (poker->streamable_container, GTK_WIDGET (combo));
	gtk_widget_show (GTK_WIDGET (combo));
	gtk_widget_show (GTK_WIDGET (poker->streamable_textview));
}

static void
streamable_import_cb (GtkButton *button, Poker *poker)
{
	GtkTextBuffer *textbuff;
	gchar *mime_type = gtk_combo_box_get_active_text (poker->streamable_combo);
	AccessibleStreamableContent *streamable =
		Accessible_getStreamableContent (poker->selected);
	char *cbuf = g_new0 (char, 1001);
	if (AccessibleStreamableContent_open (streamable, mime_type)) {
		AccessibleStreamableContent_read (streamable, cbuf, 1000, 0);
		AccessibleStreamableContent_close (streamable);
	}
	else g_message ("error opening content stream");
	textbuff = GTK_TEXT_BUFFER (gtk_text_buffer_new (NULL));
#if GTK_CHECK_VERSION (2,9,0)
	gtk_text_buffer_register_deserialize_tagset (textbuff, NULL);
	gtk_text_buffer_register_deserialize_tagset (textbuff, NULL);
#endif
	if (!strcmp ("text/plain", mime_type))
		gtk_text_buffer_set_text (textbuff, cbuf, -1);
#if GTK_CHECK_VERSION (2,9,0)
	else {
		GtkTextIter iter;
		GdkAtom atom = gdk_atom_intern (mime_type, TRUE);
		gint i, nf = 0;
		GError *err = NULL;
		GdkAtom *formats = gtk_text_buffer_get_deserialize_formats (textbuff, &nf);
		gboolean format_ok = FALSE;
		gtk_text_buffer_get_iter_at_offset (textbuff, &iter, 0);
		for (i = 0; i < nf; ++i) {
			if (!strcmp (mime_type, gdk_atom_name (formats[i]))) {
				format_ok = TRUE;
				break;
			}
		}
		if (format_ok) {
			gtk_text_buffer_deserialize_set_can_create_tags (textbuff, atom, TRUE);
			if (!gtk_text_buffer_deserialize (textbuff, textbuff, 
							  atom, 
							  &iter,
							  cbuf, 1000, &err))
			{
				g_warning ("could not deserialize mime-type %s: %s", 
					   mime_type,
					   err->message);
			}
		}
	}
#endif
	gtk_text_view_set_buffer (poker->streamable_textview, textbuff);
	g_free (cbuf);    
}

static void
process_accessible (Poker *poker, AccessibleSelection *selection, Accessible *child, GtkTreeSelection *view_sel, long i)
{
	char *name, *descr;
	GtkTreeIter    iter;
	AccessibleRole role;
	GValue         idx;
		
	name = Accessible_getName (child);
	role = Accessible_getRole (child);
	descr = Accessible_getDescription (child);

	if (!name || !name [0]) {
		AccessibleText *text = Accessible_getText (child);
		SPI_freeString (name);
		name = AccessibleText_getText (text, 0, -1);
		AccessibleText_unref (text);
	}

	memset (&idx, 0, sizeof (GValue));
	g_value_set_int (g_value_init (&idx, G_TYPE_INT), i);
	
	gtk_list_store_append (poker->selection_store, &iter);
	gtk_list_store_set (poker->selection_store, &iter,
			    POKER_SELECTION_NAME, name,
			    POKER_SELECTION_DESCR, descr,
			    POKER_SELECTION_ICON,
			    get_pixbuf_for_role (role),
			    POKER_SELECTION_IDX, &idx, -1);

	if (AccessibleSelection_isChildSelected (selection, i))
		gtk_tree_selection_select_iter (view_sel, &iter);

	SPI_freeString (descr);
	SPI_freeString (name);
	Accessible_unref (child);
}


static void
update_if_selection (Poker *poker, AccessibleSelection *selection)
{
	long i, j, k;
	Accessible *parent;
	Accessible *child;
	AccessibleTable *table;
	GtkTreeSelection *view_sel;
	gboolean use_table = FALSE;
	long n_rows, n_cols;

	parent = Accessible_queryInterface (
		selection, "IDL:Accessibility/Accessible:1.0");
	g_return_if_fail (parent != NULL);

	gtk_list_store_clear (poker->selection_store);
	view_sel = gtk_tree_view_get_selection (poker->selection_view);

	g_signal_handler_block (view_sel, poker->selection_changed_id);

	gtk_tree_selection_unselect_all (view_sel);

	if (use_table_if) {
		table = Accessible_getTable (parent);

		if (table)
			use_table = TRUE;
	}
	if (use_table) {
                if (!max_children) {
                        n_rows = AccessibleTable_getNRows (table);
                        n_cols = AccessibleTable_getNColumns (table);
                } else {
                        n_rows = max_children;
                        n_cols = max_children;
                }
                for (j = 0; j < n_rows; j++) {
                        for (k = 0; k < n_cols; k++) {

				child = AccessibleTable_getAccessibleAt (table, j, k);
				if (!child)
					break;

				i = AccessibleTable_getIndexAt (table, j, k);
				process_accessible (poker, selection, child, view_sel, i);
			}
		}
		Accessible_unref (table);
	} else {
		for (i = 0; max_children ? i < max_children : TRUE; i++) {
			child = Accessible_getChildAtIndex (parent, i);
			if (!child)
				break;
			process_accessible (poker, selection, child, view_sel, i);
		}
	}

	g_signal_handler_unblock (view_sel, poker->selection_changed_id);

	Accessible_unref (parent);
}

static void
select_accessible_cb (GtkTreeModel *model,
		      GtkTreePath  *path,
		      GtkTreeIter  *iter,
		      gpointer      data)
{
	GValue *idx;
	AccessibleSelection *selection = data;

	gtk_tree_model_get (model, iter, POKER_SELECTION_IDX, &idx, -1);

	AccessibleSelection_selectChild (selection, g_value_get_int (idx));
}

static void
selection_changed_cb (GtkTreeSelection *tree_selection, Poker *poker)
{
	AccessibleSelection *selection;

	selection = Accessible_getSelection (poker->selected);
	g_return_if_fail (selection != NULL);

	gtk_tree_selection_selected_foreach (
		tree_selection, select_accessible_cb, selection);

/*	update_if_selection (poker, selection); */

	AccessibleSelection_unref (selection);
}

static void
selection_clear_clicked_cb (GtkButton *button, Poker *poker)
{
	AccessibleSelection *selection;

	g_return_if_fail (poker->selected != NULL);

	selection = Accessible_getSelection (poker->selected);
	g_return_if_fail (selection != NULL);

	AccessibleSelection_clearSelection (selection);

	update_if_selection (poker, selection);

	AccessibleSelection_unref (selection);
}

static void
selection_all_clicked_cb (GtkButton *button, Poker *poker)
{
	AccessibleSelection *selection;

	g_return_if_fail (poker->selected != NULL);

	selection = Accessible_getSelection (poker->selected);
	g_return_if_fail (selection != NULL);

	AccessibleSelection_selectAll (selection);

	update_if_selection (poker, selection);

	AccessibleSelection_unref (selection);
}

static void
update_if_hypertext (Poker *poker, AccessibleHypertext *hypertext)
{
	long i;
	long num_links;
	GtkTreeIter iter;
	AccessibleText *text;

	num_links = AccessibleHypertext_getNLinks (hypertext);
	gtk_list_store_clear (poker->hypertext_store);

	text = Accessible_getText (hypertext);
	g_return_if_fail (text);

	for (i = 0; i < num_links; i++) {
		AccessibleHyperlink *link;
		long n_anchors;
		long start_index, end_index;
		char *txt;

		link = AccessibleHypertext_getLink (hypertext, i);
		n_anchors = AccessibleHyperlink_getNAnchors (link);

		AccessibleHyperlink_getIndexRange (link, &start_index, &end_index);
		txt = AccessibleText_getText (text, start_index, end_index);
		gtk_list_store_append (poker->hypertext_store, &iter);	
		gtk_list_store_set (poker->hypertext_store, &iter,
				    0, i,
				    1, n_anchors,
				    2, txt,
				    -1);
		SPI_freeString (txt);

		Accessible_unref (link);
	}
	Accessible_unref (text);
}

static const struct {
	const char *widget_name;
	const char *interface_name;
	const char *interface_sym;
	void (*update_fn) (Poker *poker, Accessible *accessible);
} poker_widget_names[] = {
	{ "poker_accessible_frame",    "IDL:Accessibility/Accessible:1.0",   "A", update_if_accessible },
	{ "poker_application_label",   "IDL:Accessibility/Application:1.0",  "ap", NULL },
	{ "poker_streamable_frame",    "IDL:Accessibility/StreamableContent:1.0",  "st", update_if_streamable },
	{ "poker_component_frame",     "IDL:Accessibility/Component:1.0",    "co", update_if_component },
	{ "poker_action_frame",        "IDL:Accessibility/Action:1.0",       "ac", update_if_action },
	{ "poker_text_frame",          "IDL:Accessibility/Text:1.0",         "te", update_if_text },
	{ "poker_editable_text_frame", "IDL:Accessibility/EditableText:1.0", "ed", update_if_editable },
	{ "poker_value_frame",         "IDL:Accessibility/Value:1.0",        "va", update_if_value },
	{ "poker_table_frame",         "IDL:Accessibility/Table:1.0",        "ta", update_if_table },
	{ "poker_image_frame",         "IDL:Accessibility/Image:1.0",        "im", update_if_image },
	{ "poker_selection_frame",     "IDL:Accessibility/Selection:1.0",    "se", update_if_selection },
	{ "poker_hypertext_frame",     "IDL:Accessibility/Hypertext:1.0",    "hy", update_if_hypertext },
	{ "poker_document_frame",      "IDL:Accessibility/Document:1.0",     "do", update_if_document },
	{ NULL, NULL, NULL, NULL }
};

char *
accessible_get_iface_string (Accessible *accessible)
{
	int i;
	GString *str;

	g_return_val_if_fail (accessible != NULL, NULL);

	str = g_string_new ("");
	for (i = 0; poker_widget_names [i].interface_name; i++) {
		AccessibleUnknown *unknown;

		unknown = Accessible_queryInterface (accessible, poker_widget_names [i].interface_name);

		if (unknown) {
			if (str->len)
				g_string_append (str, "|");
			g_string_append (str, poker_widget_names [i].interface_sym);
			Accessible_unref (unknown);
		}
	}

	return g_string_free (str, FALSE);
}

static void
update_frame_visibility (Poker *poker, Accessible *accessible)
{
	int i;
	
	for (i = 0; poker_widget_names [i].interface_name; i++) {
		GtkWidget *frame;
		AccessibleUnknown *unknown;

		unknown = Accessible_queryInterface (
			accessible, poker_widget_names [i].interface_name);

		if (poker_widget_names [i].widget_name)
			frame = glade_xml_get_widget (
				poker->xml, poker_widget_names [i].widget_name);
		else
			frame = NULL;

		if (unknown) {
			if (poker_widget_names [i].update_fn)
				poker_widget_names [i].update_fn (poker, unknown);
			if (frame)
				gtk_widget_show (frame);
		} else {
			if (frame)
				gtk_widget_hide (frame);
		}

		Accessible_unref (unknown);
	}
}

static void
poker_selected_object_update (AccessibleListener *listener,
			      Poker              *poker)
{
	update_frame_visibility (poker, poker->selected);
}

static void
poker_selected_object_text_update (AccessibleListener *listener,
				   Poker              *poker)
{
	update_text (poker);
}

static void
poker_tree_selection_changed (GtkTreeSelection *selection,
			      Poker            *poker)
{
	GtkTreeIter iter;
	Accessible *accessible;
	GtkTreeModel *model;
	
	if (!gtk_tree_selection_get_selected (selection, &model, &iter)) {
		poker->selected = NULL;
		return;
	}

	accessible = accessible_tree_get_from_iter (&iter);

	poker->selected = accessible;

	accessible_listener_set_target (accessible_listener_get (),
					accessible);
	
	update_frame_visibility (poker, accessible);
}

static void
poker_tree_row_collapsed (GtkTreeView *view, GtkTreeIter *iter, GtkTreePath *path, Poker *poker)
{
	if (!poker->selected) {
		gtk_tree_selection_select_iter (gtk_tree_view_get_selection (view), iter);
	}
}

static void
refresh_clicked_cb (GtkButton *button, Poker *poker)
{
	update_frame_visibility (poker, poker->selected);
}

static void
expand_clicked_cb (GtkButton *button, Poker *poker)
{
	GtkTreeIter   iter;
	GtkTreePath  *path;
	GtkTreeModel *model;

	g_return_if_fail (poker->selected != NULL);

	gtk_tree_selection_get_selected (
		gtk_tree_view_get_selection (poker->tree_view),
		&model, &iter);

	path = gtk_tree_model_get_path (model, &iter);

	gtk_tree_view_expand_row (poker->tree_view, path, TRUE);

	gtk_tree_path_free (path);
}

static gboolean
poker_window_activate_cb (GtkWindow     *window,
			  GdkEventFocus *event,
			  Poker         *poker)
{
	static gint local_changes_blocked = 0;
	if (event->in) {
		g_signal_handler_block (accessible_listener_get(),
					poker->update_id);
		g_signal_handler_block (accessible_listener_get(),
					poker->text_update_id);
		if (local_changes_blocked) {
			g_signal_handler_unblock (poker->text,
						  poker->text_changed_id);
			g_signal_handler_unblock (poker->text_caret,
						  poker->caret_changed_id);
			--local_changes_blocked;
		}
	} else {
		++local_changes_blocked;
		g_signal_handler_block (poker->text,
					poker->text_changed_id);
		g_signal_handler_block (poker->text_caret,
					poker->caret_changed_id);
		g_signal_handler_unblock (accessible_listener_get(),
					  poker->update_id);
		g_signal_handler_unblock (accessible_listener_get(),
					  poker->text_update_id);
	}
	return TRUE;
}

static void
add_pix_name_column (GtkTreeView *tree_view,
		     int          icon_col,
		     int          name_col)
{
	GtkCellRenderer   *cell;
	GtkTreeViewColumn *column;

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_reorderable (column, TRUE);
	gtk_tree_view_column_set_title (column, "Name");

	cell = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, cell, FALSE);
	gtk_tree_view_column_set_attributes (
		column, cell, "pixbuf", icon_col, NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);

	cell = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, cell, TRUE);
	gtk_tree_view_column_set_attributes (
		column, cell, "text", name_col, NULL);
	gtk_tree_view_column_set_sort_column_id (column, name_col);
	gtk_tree_view_append_column (tree_view, column);
}

static void
init_accessible_tree (Poker *poker, Accessible *accessible)
{
	GtkTreeIter iter;

	poker->tree_model = accessible_tree_model_new (accessible);

	poker->tree_view = GTK_TREE_VIEW (
		glade_xml_get_widget (
			poker->xml, "poker_tree_view"));

	gtk_tree_view_set_model (poker->tree_view, poker->tree_model);

	add_pix_name_column (poker->tree_view,
			     ACCESSIBLE_TREE_MODEL_ICON_COL,
			     ACCESSIBLE_TREE_MODEL_NAME_COL);

	gtk_tree_view_insert_column_with_attributes
		(poker->tree_view,
		 ACCESSIBLE_TREE_MODEL_ROLE_COL, "Role",
		 gtk_cell_renderer_text_new (),
		 "text", 1,
		 NULL);

	gtk_tree_view_insert_column_with_attributes
		(poker->tree_view,
		 ACCESSIBLE_TREE_MODEL_DESCR_COL, "Description",
		 gtk_cell_renderer_text_new (),
		 "text", 2,
		 NULL);

	gtk_tree_view_columns_autosize (poker->tree_view);
	gtk_tree_selection_set_mode (
		gtk_tree_view_get_selection (
			poker->tree_view),
		GTK_SELECTION_BROWSE);
	g_signal_connect (
		gtk_tree_view_get_selection (poker->tree_view),
		"changed",
		G_CALLBACK (poker_tree_selection_changed),
		poker);

	g_signal_connect (
		poker->tree_view,
		"row-collapsed",
		G_CALLBACK (poker_tree_row_collapsed),
		poker);

	if (gtk_tree_model_get_iter_root (
		GTK_TREE_MODEL (poker->tree_model), &iter)) {
		GtkTreePath *path;

		gtk_tree_selection_select_iter (
			gtk_tree_view_get_selection (poker->tree_view), &iter);

		path = gtk_tree_model_get_path (poker->tree_model, &iter);
		gtk_tree_view_expand_row (poker->tree_view, path, FALSE);
		gtk_tree_path_free (path);
	}
}

static void
poker_destroy (Poker *poker)
{
	poker_list = g_list_remove (poker_list, poker);

	g_signal_handler_disconnect (accessible_listener_get(),
				     poker->update_id);
	g_signal_handler_disconnect (accessible_listener_get(),
				     poker->text_update_id);

	child_listener_remove_root (child_listener_get (), poker->root_node);

	gtk_widget_destroy (poker->window);

	g_object_unref (poker->tree_model);	
	g_object_unref (poker->xml);
	g_free (poker);
}

static void
root_died_cb (ChildListener *listener, Accessible *root, Poker *poker)
{
	poker_destroy (poker);
}

static gboolean
window_delete_event (GtkWidget   *widget,
		     GdkEventAny *event,
		     Poker       *poker)
{
	poker_destroy (poker);
	return TRUE;
}

static Poker *
poker_create (Accessible *accessible)
{
	Poker *poker;
	GtkWidget *widget;

	poker = g_new0 (Poker, 1);
	
	/* FIXME: need a toggle for this */
	poker->ctype = SPI_COORD_TYPE_SCREEN;

	poker->root_node = accessible;
	poker->xml = get_glade_xml ();

	{ /* accessible interface */
		poker->accessible_role = glade_xml_get_widget (
			poker->xml, "accessible_if_role");
		poker->accessible_role_image = glade_xml_get_widget (
			poker->xml, "accessible_if_role_image");
		poker->accessible_relations_frame = glade_xml_get_widget (
			poker->xml, "accessible_if_relations_frame");
		poker->accessible_states_frame = glade_xml_get_widget (
			poker->xml, "accessible_if_states_frame");

		poker->state_set_store = gtk_list_store_new (1, G_TYPE_STRING);
		widget = glade_xml_get_widget (poker->xml, "accessible_if_state_list");
		gtk_tree_view_set_model (GTK_TREE_VIEW (widget),
					 GTK_TREE_MODEL (poker->state_set_store));
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 0, "State name",
			gtk_cell_renderer_text_new (), "text", 0, NULL);

		poker->relation_store = gtk_list_store_new (
			3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
		widget = glade_xml_get_widget (poker->xml, "accessible_if_relation_list");
		gtk_tree_view_set_model (GTK_TREE_VIEW (widget),
					 GTK_TREE_MODEL (poker->relation_store));
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), RELATION_NAME, "relation",
			gtk_cell_renderer_text_new (), "text", 0, NULL);
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), RELATION_LABEL, "label",
			gtk_cell_renderer_text_new (), "text", 1, NULL);
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), RELATION_PATH, "path",
			gtk_cell_renderer_text_new (), "text", 2, NULL);
		g_signal_connect (widget, "row_activated",
				  G_CALLBACK (relation_row_activated_cb),
				  poker);
	}

	{ /* component interface */
		g_signal_connect (
			glade_xml_get_widget (poker->xml, "component_if_grab_focus"),
			"clicked",
			G_CALLBACK (component_grab_focus_clicked),
			poker);
	}

	{ /* action interface */
		GtkTreeView *treeview;

		poker->action_store = gtk_list_store_new (
			3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);

		poker->action_view = widget = glade_xml_get_widget (
			poker->xml, "action_if_list");

		treeview = GTK_TREE_VIEW (widget);
		gtk_tree_view_set_model (treeview,
					 GTK_TREE_MODEL (poker->action_store));

		gtk_tree_view_insert_column_with_attributes (
			treeview, 0, "Name",
			gtk_cell_renderer_text_new (), "text", 0, NULL);
		gtk_tree_view_insert_column_with_attributes (
			treeview, 1, "Descr",
			gtk_cell_renderer_text_new (), "text", 1, NULL);
		gtk_tree_view_insert_column_with_attributes (
			treeview, 2, "Keybinding",
			gtk_cell_renderer_text_new (), "text", 2, NULL);
		
		g_signal_connect (widget, "row_activated",
				  G_CALLBACK (action_row_activated_cb),
				  poker);

		poker->action_take = glade_xml_get_widget (
			poker->xml, "action_if_take");
		g_signal_connect (
			poker->action_take, "clicked",
			G_CALLBACK (action_take_clicked), poker);
	}

	{ /* text interface */
		poker->text = glade_xml_get_widget (
			poker->xml, "text_if_entry");
		poker->text_changed_id = 
			g_signal_connect (poker->text, "changed",
					  G_CALLBACK (editable_text_changed_cb),
					  poker);

		poker->text_caret = glade_xml_get_widget (
			poker->xml, "text_if_caret");
		poker->text_frame_label = GTK_LABEL (glade_xml_get_widget
						     (poker->xml, "text_if_frame_label") );
		poker->caret_changed_id =
			g_signal_connect (poker->text_caret,
				  "value_changed",
				  G_CALLBACK (text_caret_changed_cb),
				  poker);
		poker->attribute_label = glade_xml_get_widget (
			poker->xml, "attribute_label");
	}

	{ /* editable text interface */
		g_signal_connect (
			glade_xml_get_widget (
				poker->xml, "editable_if_cut"),
			"clicked",
			G_CALLBACK (editable_cut_clicked_cb),
			poker);
		g_signal_connect (
			glade_xml_get_widget (
				poker->xml, "editable_if_copy"),
			"clicked",
			G_CALLBACK (editable_copy_clicked_cb),
			poker);
		g_signal_connect (
			glade_xml_get_widget (
				poker->xml, "editable_if_paste"),
			"clicked",
			G_CALLBACK (editable_paste_clicked_cb),
			poker);
		g_signal_connect (
			glade_xml_get_widget (
				poker->xml, "editable_if_delete"),
			"clicked",
			G_CALLBACK (editable_delete_clicked_cb),
			poker);
	}

	{ /* value interface */
		poker->value_minimum = glade_xml_get_widget (
			poker->xml, "value_if_minimum");
		poker->value_current = glade_xml_get_widget (
			poker->xml, "value_if_current");
		poker->value_maximum = glade_xml_get_widget (
			poker->xml, "value_if_maximum");
		poker->value_changed_id = 
			g_signal_connect (poker->value_current,
					  "changed",
					  G_CALLBACK (value_text_changed_cb),
					  poker);
	}

	{ /* table interface */
		poker->table_rows_store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
		widget = glade_xml_get_widget (poker->xml, "table_if_row_data");
		gtk_tree_view_set_model (GTK_TREE_VIEW (widget),
					 GTK_TREE_MODEL (poker->table_rows_store));
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 0, "Row Description",
			gtk_cell_renderer_text_new (), "text", 0, NULL);
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 1, "Height",
			gtk_cell_renderer_text_new (), "text", 1, NULL);

		poker->table_columns_store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
		widget = glade_xml_get_widget (poker->xml, "table_if_column_data");
		gtk_tree_view_set_model (GTK_TREE_VIEW (widget),
					 GTK_TREE_MODEL (poker->table_columns_store));
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 0, "Column Description",
			gtk_cell_renderer_text_new (), "text", 0, NULL);
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 1, "Width",
			gtk_cell_renderer_text_new (), "text", 1, NULL);
	}

	{ /* image interface */
		poker->image_descr = glade_xml_get_widget (
			poker->xml, "image_if_description");
		/* Prevent width of image description expanding too much.*/
		gtk_widget_set_size_request (poker->image_descr, 100, -1);
		poker->image_size  = glade_xml_get_widget (
			poker->xml, "image_if_size");
	}

	{ /* selection interface */
		poker->selection_store = gtk_list_store_new (
			4, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_VALUE);

		widget = glade_xml_get_widget (
			poker->xml, "selection_if_children");
		poker->selection_view = GTK_TREE_VIEW (widget); 

		gtk_tree_view_set_model (GTK_TREE_VIEW (widget),
					 GTK_TREE_MODEL (poker->selection_store));
		gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (widget), FALSE);
		add_pix_name_column (GTK_TREE_VIEW (widget),
				     POKER_SELECTION_ICON,
				     POKER_SELECTION_NAME);
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 2, "Descr",
			gtk_cell_renderer_text_new (), "text",
			POKER_SELECTION_DESCR, NULL);

		poker->selection_all = glade_xml_get_widget (
			poker->xml, "selection_if_all");
		poker->selection_clear = glade_xml_get_widget (
			poker->xml, "selection_if_clear");

		poker->selection_changed_id = g_signal_connect (
			gtk_tree_view_get_selection (poker->selection_view),
			"changed", G_CALLBACK (selection_changed_cb), poker);
		g_signal_connect (
			glade_xml_get_widget (poker->xml, "selection_if_clear"),
			"clicked", G_CALLBACK (selection_clear_clicked_cb), poker);
		g_signal_connect (
			glade_xml_get_widget (poker->xml, "selection_if_all"),
			"clicked", G_CALLBACK (selection_all_clicked_cb), poker);
	}

	{ /* hypertext interface */
		poker->hypertext_store = gtk_list_store_new (
			3, G_TYPE_INT, G_TYPE_INT, G_TYPE_STRING);

		widget = glade_xml_get_widget (
			poker->xml, "hypertext_if_links");
		poker->hypertext_view = GTK_TREE_VIEW (widget); 

		gtk_tree_view_set_model (GTK_TREE_VIEW (widget),
					 GTK_TREE_MODEL (poker->hypertext_store));
		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 0, "Link",
			gtk_cell_renderer_text_new (), "text",
			0, NULL);

		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 1, "Anchors",
			gtk_cell_renderer_text_new (), "text",
			1, NULL);

		gtk_tree_view_insert_column_with_attributes (
			GTK_TREE_VIEW (widget), 2, "Name",
			gtk_cell_renderer_text_new (), "text",
			2, NULL);

		g_signal_connect (widget, "row_activated",
				  G_CALLBACK (link_activated_cb),
				  poker);
	}

 	{ /* document interface */
 		poker->document_locale  = glade_xml_get_widget (
                         poker->xml, "document_if_locale");
 		poker->document_attributes  = glade_xml_get_widget (
                         poker->xml, "document_if_attributes");
  	}
  
	{ /* streamable-content interface */
		widget = glade_xml_get_widget (
			poker->xml, "streamable_vbox");
		poker->streamable_container = GTK_CONTAINER (widget);
		widget = glade_xml_get_widget (
			poker->xml, "streamable_viewport");
		if (!poker->streamable_textview) {
			poker->streamable_textview = 
				GTK_TEXT_VIEW (gtk_text_view_new ());
			gtk_text_view_set_wrap_mode (poker->streamable_textview,
						     GTK_WRAP_WORD);
			gtk_container_add (GTK_CONTAINER (widget), 
					   GTK_WIDGET (poker->streamable_textview));
		}
		widget = glade_xml_get_widget (
			poker->xml, "streamable_launch_button");
		gtk_widget_set_sensitive (widget, FALSE);
		g_signal_connect (
			glade_xml_get_widget (poker->xml, "streamable_text_import_button"),
			"clicked", G_CALLBACK (streamable_import_cb), poker);
	}

	{ /* main frame buttons */
		g_signal_connect (
			glade_xml_get_widget (
				poker->xml, "poker_refresh"),
			"clicked",
			G_CALLBACK (refresh_clicked_cb),
			poker);

		g_signal_connect (
			glade_xml_get_widget (
				poker->xml, "poker_expand"),
			"clicked",
			G_CALLBACK (expand_clicked_cb),
			poker);
	}

	{ /* main window */
		GClosure *closure;

		poker->window = glade_xml_get_widget (
			poker->xml, "poker_window");
		g_object_add_weak_pointer (G_OBJECT (poker->window), 
				(gpointer *) &(poker->window));

		closure = g_cclosure_new (
			G_CALLBACK (root_died_cb), poker, NULL);
		g_object_watch_closure (
			G_OBJECT (poker->window), closure);
		g_signal_connect_closure (
			child_listener_get (), "root_died", closure, FALSE);

		child_listener_add_root (child_listener_get (), poker->root_node);
		poker->update_id = g_signal_connect (
			accessible_listener_get (),
			"object-update",
			G_CALLBACK (poker_selected_object_update),
			poker);
		
		poker->text_update_id = g_signal_connect (
			accessible_listener_get (),
			"text-update",
			G_CALLBACK (poker_selected_object_text_update),
			poker);

		g_signal_connect (
			poker->window,
			"focus_in_event",
			G_CALLBACK (poker_window_activate_cb),
			poker);

		g_signal_connect (
			poker->window,
			"focus_out_event",
			G_CALLBACK (poker_window_activate_cb),
			poker);

		g_signal_connect (
			poker->window,
			"delete_event",
			G_CALLBACK (window_delete_event),
			poker);
	}

	init_accessible_tree (poker, accessible);
	gtk_widget_show (poker->window);

	poker_list = g_list_prepend (poker_list, poker);

	return poker;
}

static Poker *
find_poker (Accessible *root_node)
{
	GList *l;
	for (l = poker_list; l; l = l->next) {
		Poker *poker = l->data;
		if (poker->root_node == root_node)
			return poker;
	}
	return NULL;
}

void
poke (Accessible *accessible)
{
	GtkTreePath *path;
	Poker *poker = NULL;
	Accessible *cur, *root = NULL;
	AccessibleStateSet *state_set = NULL;

	if (accessible == NULL ||
	    !(state_set = Accessible_getStateSet (accessible)) ||
	    AccessibleStateSet_contains (state_set, SPI_STATE_DEFUNCT) )
	{
		GtkWidget *msg = gtk_message_dialog_new (NULL, 0,
							 GTK_MESSAGE_WARNING,
							 GTK_BUTTONS_CLOSE,
							 "This accessible appears to have become defunct");
	    gtk_dialog_run (GTK_DIALOG (msg));
	    gtk_widget_destroy (msg);
	    return;
	}

	path = gtk_tree_path_new ();

	for (cur = accessible; cur; cur = Accessible_getParent (cur)) {
		gboolean is_application = Accessible_getRole(cur) == SPI_ROLE_APPLICATION;
		long index_in_parent = Accessible_getIndexInParent (cur);
		
		if (index_in_parent >= 0) {
			if (path)
				gtk_tree_path_prepend_index (path, index_in_parent);
		} else if (!is_application) {
			gtk_tree_path_free (path);
			path = NULL;
		}
		
		root = cur;

		if ((poker = find_poker (cur))) {
			gtk_window_present (GTK_WINDOW (poker->window));
			break;
		}

		if (is_application)
			break;
	}
	if (!root) {
		g_warning ("Couldn't find any sane / application parent "
			   "for this accessible");
		poker = poker_create (accessible);
	}

	if (!poker)
		poker = poker_create (root);

	/* Expand down the tree to here - hopefully the tree hasn't
	   changed in the meantime */
	if (path) {
		gtk_tree_path_prepend_index (path, 0);

		gtk_tree_view_expand_to_path (poker->tree_view, path);
		char *path_str = gtk_tree_path_to_string (path);
		if (path_str) {
			poker_select_accessible (poker, path_str);
			g_free (path_str);
		}
		gtk_tree_path_free (path);
	}
}
